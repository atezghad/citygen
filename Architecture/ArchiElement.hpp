#ifndef ARCHIELEMENT_H
#define ARCHIELEMENT_H

#include <list>

#include <Utils/structs.hpp>
#include <Utils/Geometry/Plane.hpp>
#include <Architecture/archi.hpp>
#include <Shapes/Cube.hpp>

/**
 * @class ArchiElement
 * @brief Permet de représenter un élément arhitectural
 */
class ArchiElement
{
	public:
		/**
		 * @brief ArchiElement
		 * @param e Elements contenant les dimensions principales de l'utilisateur
		 */
		ArchiElement(const Element e);

		/**
		 * @brief widthSpace
		 * @return L'espace horizontal entre cet Element et un autre
		 */
		double widthSpace() const;

		/**
		 * @brief lengthSpace
		 * @return L'espace vertical entre cet Element et un autre
		 */
		double lengthSpace() const;

		/**
		 * @brief volume
		 * @return
		 */
		Volume volume() const;

		/**
		 * @brief thickness
		 * @return L'épaisseur des barreaux
		 */
		double thickness() const;

		/**
		 * @brief width
		 * @return
		 */
		double width() const;

        /**
         * @brief Permet de géométriquement créer l'element
         * @param p Le plan sur lequel l'élément sera placé
         * @param invert true si normal pointe vers l'intérieur, false sinon
         * @return La liste de cubes générés
         */
        virtual std::list<Shape3*> createElement(const Plane &p, const bool invert) = 0;

		/**
		 * @brief Permet de géométriquement placer l'element
		 * @param a Origine du plan (AB, AC)
		 * @param b Un point du plan
		 * @param c Un point du plan
		 * @param h La hauteur
		 * @param p Plan d'origine
		 * @param m La liste de cube
		 */
		void placeElement(Point3 &a,
						  const Point3 &b,
						  const Point3 &c,
						  const double &h,
						  const Plane &p,
                          std::list<Shape3*> &m);

        virtual ~ArchiElement();

	protected:
		double m_widthSpace;
		double m_lengthSpace;
		Volume m_volume;
		double m_thickness;
};

#endif // ARCHIELEMENT_H
