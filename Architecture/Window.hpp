#ifndef WINDOW_H
#define WINDOW_H

#include <Architecture/ArchiElement.hpp>

/**
 * @class Window
 * @brief Permet de représenter une fenêtre
 */
class Window : public ArchiElement
{
	public:
		Window(const Element e, const int nbCroisillonsH, const int nbCroisillonsV);

        std::list<Shape3*> createElement(const Plane &p, const bool invert) override;

        ~Window() override;

	private:
		int m_croisillons_h;
		int m_croisillons_v;

        void createLatticeH(std::list<Shape3*> &l, const Plane &p);
        void createLatticeV(std::list<Shape3*> &l, const Plane &p);
};

#endif // WINDOW_H
