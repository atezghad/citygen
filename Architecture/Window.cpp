#include <Architecture/Window.hpp>

Window::Window(const Element e, const int nbCroisillonsH, const int nbCroisillonsV)
	: ArchiElement(e), m_croisillons_h(nbCroisillonsH), m_croisillons_v(nbCroisillonsV)
{

}

void Window::createLatticeH(std::list<Shape3*> &l, const Plane &p)
{
    if(m_croisillons_h != 0)
    {
        double nb = m_volume.l / ( m_croisillons_h + 1);
        Point3 pt0 = p.center - p.x*m_volume.L/2.0 + - p.y*m_volume.l/2.0;
        pt0 += p.y*nb;
        for(int i = 0; i < m_croisillons_h; ++i)
        {
            Shape3 * crH = new Cube(pt0, 0.05, m_volume.L, 0.05, p.n);
            l.push_back(crH);
            pt0 += p.y*nb;
        }
    }
}

void Window::createLatticeV(std::list<Shape3*> &l, const Plane &p)
{
    if(m_croisillons_v != 0)
    {
        double nb = m_volume.L / ( m_croisillons_v + 1);
        Point3 pt0 = p.center - p.x*m_volume.L/2.0 + - p.y*m_volume.l/2.0;
        pt0 += p.x*nb;
        for(int i = 0; i < m_croisillons_v; ++i)
        {
            Shape3 * crH = new Cube(pt0, m_volume.l, 0.05, 0.05, p.n);
            l.push_back(crH);
            pt0 += p.x*nb;
        }
    }
}

std::list<Shape3*> Window::createElement(const Plane &p, const bool invert)
{
    std::list<Shape3*> win = archi::frame(p, m_volume, m_thickness);
    createLatticeH(win, p);
    createLatticeV(win, p);
    if(invert)
    {
        for(std::list<Shape3*>::iterator it = win.begin(); it != win.end(); ++it)
            (*it)->move(-p.n*m_thickness);
    }
    return win;
}

Window::~Window()
{

}
