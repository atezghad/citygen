#ifndef PROPS_HPP
#define PROPS_HPP

#include <list>
#include <Shapes/Cube.hpp>
#include <Shapes/Shape2.hpp>
#include <Utils/Geometry/Plane.hpp>

/**
 * \namespace archi
 * \brief Contient plusieurs fonctions et attributs liés aux éléments architecturaux
 * */
namespace archi
{

    /**
     * @brief Permet de créer un cadre
     * @param p Plan
     * @param v Volume du cadre
     * @param bordure Epaisseur des montants
     * @return Liste de pavés droits générées
     */
    std::list<Shape3*> frame(const Plane &p,
                          const Volume &v,
                          const double &bordure);

    /**
     * @brief Permet de créer les bordures d'un toit
     * @param base Forme de base
     * @param height Hauteur des bords
     * @return Liste de pavés droits générées
     */
    std::list<Shape3*> roofBorders(Shape2 *base, const double &height);

	const int DOOR   = 1 ;
	const int WINDOW = 2 ;
	const int BALCON = 3 ;
	const int COLUMN = 4 ;
}
#endif // PROPS_HPP
