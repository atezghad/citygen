#include "Column.hpp"

Column::Column(const Element e) : ArchiElement(e)
{}

std::list<Shape3*> Column::createElement(const Plane &p, const bool invert)
{
    Point3 pt0 = p.center + p.x*(-m_volume.L/2.0 + m_volume.h) + p.y*(-m_volume.l/2.0 + m_volume.h);
    double l = Vec3(pt0, p.center).y()*2.0;
    Shape3 * column = new Cube(pt0, l, m_volume.L + 2.0*m_widthSpace, m_volume.h, p.n);
    if(invert)
    {
        column->move(-p.n*m_volume.h);
    }
    return std::list<Shape3*>({column});
}

Column::~Column()
{

}
