#include <Architecture/Balcon.hpp>
#include <Utils/utils.hpp>


Balcon::Balcon(const Element e, const int nbBarresH, const int nbBarresV) :
	ArchiElement(e), m_barresH(nbBarresH), m_barresV(nbBarresV)
{

}

std::list<Shape3*> Balcon::createElement(const Plane &p, const bool invert)
{
    std::list<Shape3*> m = archi::frame(p, {m_volume.l, m_volume.L, m_thickness }, m_thickness);
    Point3 pt0 = p.center + p.x*(-m_volume.L/2.0) + p.y*(-m_volume.l/2.0);
    Shape3* sol = new Cube(pt0, m_thickness, m_volume.L, m_volume.h, p.n);
    Point3 pt1 = pt0 + p.x*(m_volume.L - m_thickness) + p.n*m_volume.h;
    Point3 pt2 = pt0 + p.x*(m_volume.L - m_thickness) + p.y*m_volume.l;
    Point3 pt3 = pt0 + p.y*m_volume.l + p.n*m_volume.h;
    std::list<Shape3*> m2 = archi::frame(  { utils::geom::center(pt1, pt2), -p.n, p.y, p.x, true },
                                        { m_volume.l, m_volume.h, m_thickness },
                                        m_thickness);
    std::list<Shape3*> m3 = archi::frame(  { utils::geom::center(pt0, pt3), -p.n, p.y, p.x, true },
                                        { m_volume.l, m_volume.h, m_thickness },
                                        m_thickness);
    if(invert)
    {
        m.push_back(sol);
        m.splice(m.end(), m2);
        m.splice(m.end(), m3);
        for(std::list<Shape3*>::iterator it = m.begin(); it != m.end(); ++it)
            (*it)->move(-p.n*m_volume.h);
    }
    else
    {
        for(std::list<Shape3*>::iterator it = m.begin(); it != m.end(); ++it)
            (*it)->move(p.n*m_volume.h);
        m.push_back(sol);
        m.splice(m.end(), m2);
        m.splice(m.end(), m3);
    }
    return m;
}

Balcon::~Balcon()
{

}
