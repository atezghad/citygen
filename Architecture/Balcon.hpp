#ifndef BALCON_H
#define BALCON_H

#include <Architecture/ArchiElement.hpp>

/**
 * @class Balcon
 * @brief Permet de représenter un balcon
 */
class Balcon : public ArchiElement
{
	public:
		Balcon(const Element e, const int nbBarresH, const int nbBarresV);

        std::list<Shape3*> createElement(const Plane &p, const bool invert) override;

        ~Balcon() override;

	private:
		int m_barresH;
		int m_barresV;
};

#endif // BALCON_H
