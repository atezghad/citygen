#ifndef DOOR_H
#define DOOR_H

#include <Architecture/ArchiElement.hpp>

/**
 * @class Door
 * @brief Permet de représenter une porte
 */
class Door : public ArchiElement
{
	public:
		Door(const Element e, const Point3 &positionPoignee);

        std::list<Shape3*> createElement(const Plane &p, const bool invert) override;

        ~Door() override;
    private:
		Point3 m_positionPoignee;
};

#endif // DOOR_H
