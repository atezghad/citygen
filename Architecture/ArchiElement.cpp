#include "ArchiElement.hpp"
#include <Utils/utils.hpp>

ArchiElement::ArchiElement(const Element e) :
	m_widthSpace(e.widthSpace),
	m_lengthSpace(e.lengthSpace),
	m_volume(e.volume),
	m_thickness(e.thickness)

{}

double ArchiElement::widthSpace() const
{
	return m_widthSpace;
}

double ArchiElement::lengthSpace() const
{
	return m_lengthSpace;
}

Volume ArchiElement::volume() const
{
	return m_volume;
}

double ArchiElement::thickness() const
{
	return m_thickness;
}

double ArchiElement::width() const
{
	return m_volume.L + 2.0*m_widthSpace;
}

void ArchiElement::placeElement(Point3 &a, const Point3 &b, const Point3 &c, const double &h, const Plane &p, std::list<Shape3*> &m)
{
    double L = m_volume.L + 2.0*m_widthSpace;
    if(a.distance(b) > L && a.distance(c) > (m_volume.l + 2.0*m_lengthSpace))
    {
        Point3 b0 = a + L*p.x;
        Point3 d = Point3(b0.x(), h, b0.z());
        Point3 mid = utils::geom::center(a, d);
        std::list<Shape3*> elem = createElement({mid, p.x, p.y, p.n, true}, p.normalInside);
        m.splice(m.end(), elem);
        a += L*p.x ;
    }
}

ArchiElement::~ArchiElement()
{

}
