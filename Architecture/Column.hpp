#ifndef COLUMN_H
#define COLUMN_H

#include <Architecture/ArchiElement.hpp>

/**
  @class Column
 * @brief Permet de représenter une colonne
 */
class Column : public ArchiElement
{
	public:
		Column(const Element e);

        std::list<Shape3*> createElement(const Plane &p, const bool invert) override;

        ~Column() override;
};

#endif // COLUMN_H
