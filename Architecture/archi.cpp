#include <Architecture/archi.hpp>
#include <Utils/utils.hpp>

std::list<Shape3*> archi::frame(const Plane &p,
                      const Volume &v,
                      const double &bordure)
{
    Point3 pt0 = p.center + p.x*(-v.L/2.0 + bordure) + p.y*( v.l/2.0 - bordure);
    Point3 pt1 = p.center + p.x*(-v.L/2.0 + bordure) + p.y*(-v.l/2.0);
    Point3 pt2 = p.center + p.x*(-v.L/2.0)           + p.y*(-v.l/2.0);
    Point3 pt3 = p.center + p.x*( v.L/2.0 - bordure) + p.y*(-v.l/2.0);

    Shape3 * bord0 = new Cube(pt0, bordure, v.L - 2.0*bordure, v.h, p.n);
    Shape3 * bord1 = new Cube(pt1, bordure, v.L - 2.0*bordure, v.h, p.n);
    Shape3 * bord2 = new Cube(pt2, v.l, bordure, v.h, p.n);
    Shape3 * bord3 = new Cube(pt3, v.l, bordure, v.h, p.n);
    std::list<Shape3*> m({bord0, bord1, bord2, bord3});
    return m;
}

std::list<Shape3*> archi::roofBorders(Shape2 *base, const double &height)
{
    Shape2 * b(base);
    b->shrink(-0.5);
    Point3 a;
    Vec3 n;
    std::vector<Point2> m_points = b->points();
    std::vector<Point3> in  ;
    std::vector<Vec2> normals = b->normals() ;
    unsigned int nbpoints = m_points.size();
    std::list<Shape3*> m;
    for(unsigned int i = 0; i < nbpoints; ++i)
    {
        a = Point3(m_points[i].x(), height, m_points[i].y() );
        n = Vec3(normals.at(i).x(), 0, normals.at(i).y());
        in.push_back(a + 0.25 * n);
    }
    for(unsigned int i = 0; i < nbpoints; ++i)
    {
        Shape3 * bord = new Cube(Point3(m_points[i].x(), height, m_points[i].y()),
                  Point3(m_points[(i+1) % nbpoints].x(), height, m_points[(i+1) % nbpoints].y()),
                  in[(i+1) % nbpoints],
                  in[i], 0.5, Vec3::Y);

        m.push_back(bord);
    }
    int nbRoofElem = RandomGen::getInstance().getInt(0,5);
    for(int i = 0; i < nbRoofElem; ++i)
    {
        Point2 p;
        do
        {
            p = base->randomPoint();
        }
        while(!base->inside(p));
        Point3 selected(p.x(), height, p.y());
        Shape3 * elem = new Cube(selected,
                  RandomGen::getInstance().getDouble(1, 2),
                  RandomGen::getInstance().getDouble(1, 2),
                  RandomGen::getInstance().getDouble(1, 2),
                  Vec3::Y);

        m.push_back(elem);
    }
    return m;
}
