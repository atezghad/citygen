#include <Architecture/Door.hpp>


Door::Door(const Element e, const Point3 &positionPoignee) :
	ArchiElement(e), m_positionPoignee(positionPoignee)
{
}

std::list<Shape3*> Door::createElement(const Plane &p, const bool invert)
{
    (void)invert;
    std::list<Shape3*> porte = archi::frame(p, {m_volume.l, m_volume.L, m_thickness }, m_thickness);
    Point3 pt0 = p.center + p.x*(-m_volume.L/2.0 + m_thickness) + p.y*(-m_volume.l/2.0 + m_thickness);
    Shape3 * ouvrant = new Cube(pt0, m_volume.l-2*m_thickness, m_volume.L-2*m_thickness, m_volume.h/2, p.n);
    porte.push_back(ouvrant);
    Point3 p_bottom = p.center - p.y*m_volume.l/2.0;
    Point3 p_sol(p_bottom.x(), 0, p_bottom.z());
    Vec3 toG(p_bottom, p_sol);
    for(std::list<Shape3*>::iterator it = porte.begin(); it != porte.end(); ++it)
        (*it)->move(toG);
    return porte;
}

Door::~Door()
{

}
