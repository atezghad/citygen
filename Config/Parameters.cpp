#include <iostream>
#include <fstream>
#include <cassert>
#include <cmath>

#include <Config/Parameters.hpp>
#include <Utils/utils.hpp>

std::map<std::string, double> Parameters::m_parameters;

void Parameters::initParameters(const std::string &filename)
{
	std::ifstream fichier;
	std::string ligne;
	fichier.open(filename.c_str());
	if(fichier.is_open())
	{
		std::vector<std::string> champs;
		do
		{
			ligne = utils::noComment(fichier);
			if(!ligne.empty())
			{
				champs = utils::split(ligne, ':');
				assert(champs.size() == 2);
				assert(!champs.at(0).empty());
				assert(!champs.at(1).empty());
				m_parameters.insert(std::make_pair(champs.at(0), std::stod(champs.at(1))));
			}

		}
		while(!ligne.empty());
		fichier.close();
	}
	else
	{
		m_parameters.insert(std::make_pair("TRIANGLE_INDEX", 1.8));
		m_parameters.insert(std::make_pair("QUAD_INDEX",     1.5));
		m_parameters.insert(std::make_pair("GAP",            1.0));
		m_parameters.insert(std::make_pair("DELTA_INDEX",    1.0));
		m_parameters.insert(std::make_pair("MIN_AREA",       100));
		m_parameters.insert(std::make_pair("MIN_SUBAREA",    50));
		m_parameters.insert(std::make_pair("PAVEMENT_WIDTH", 2.2));
		m_parameters.insert(std::make_pair("ROAD_WIDTH",     2.48));
		m_parameters.insert(std::make_pair("SAFETY",         1.0));
		std::cout << "Fichier de paramètres manquant, paramètres par défaut utilisés." << std::endl;
	}
}

Parameters::Parameters()
{}

Parameters& Parameters::getInstance()
{
	static Parameters instance;
	return instance;
}

double Parameters::get(const std::string &param)
{
	std::map<std::string, double>::iterator p = m_parameters.find(param);
	if(p == m_parameters.end())
	{
		std::cerr << "Paramètre introuvable, renvoie 1.0" << std::endl;
		return 1.0;
	}
	else return p->second;
}
