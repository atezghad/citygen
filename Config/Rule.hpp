#ifndef RULE_HPP
#define RULE_HPP


template<typename T>
/**
 * @brief Permet de représenter une règle de grammaire probabiliste
 */
class Rule
{
	public:

		Rule();

		/**
		 * @brief Rule
		 * @param symbol symbole
		 * @param axiomes L'ensemble des symboles constituant la règle
		 * @param proba probabilité entre 0 et 1
		 */
		Rule(const char symbol, T axiomes, const float &proba);

		/**
		 * @brief Retourne la probabilité de la règle
		 * @return La probabilité de la règle
		 */
		float probability() const;

		/**
		 * @brief Retourne l'ensemble des symboles constituant la règle
		 * @return L'ensemble des symboles constituant la règle
		 */
		T axiomes();

		/**
		 * @brief symbol
		 * @return
		 */
		char symbol() const;

		/**
		 * @brief setAxiomes
		 * @param axiomes
		 */
		void setAxiomes(const T &axiomes);

		/**
		 * @brief setProbability
		 * @param probability
		 */
		void setProbability(float probability);

		/**
		 * @brief setSymbol
		 * @param symbol
		 */
		void setSymbol(char symbol);

	private:
		T     _axiomes;
		float _probability;
		char  _symbol;
};

#endif // RULE_HPP

template<typename T>
Rule<T>::Rule() : _axiomes(), _probability(0)
{}

template<typename T>
Rule<T>::Rule(const char symbol, T axiomes, const float &proba) : _axiomes(axiomes), _probability(proba), _symbol(symbol)
{}

template<typename T>
void Rule<T>::setAxiomes(const T &axiomes)
{
	_axiomes = T(axiomes);
}

template<typename T>
void Rule<T>::setProbability(float probability)
{
	_probability = probability;
}

template<typename T>
void Rule<T>::setSymbol(char symbol)
{
	_symbol = symbol;
}

template<typename T>
float Rule<T>::probability() const
{
	return _probability;
}

template<typename T>
T Rule<T>::axiomes()
{
	return _axiomes;
}

template<typename T>
char Rule<T>::symbol() const
{
	return _symbol;
}
