#include <fstream>

#include <Config/Grammar.hpp>

Grammar::Grammar()
{
}

Grammar& Grammar::getInstance()
{
	static Grammar instance;
	return instance;
}

void Grammar::initGrammar(const std::string &filename)
{
	std::ifstream fichier;
	std::string ligne;

	fichier.open(filename.c_str());
	if(fichier.is_open())
	{
		std::vector<std::string> champs;
		std::vector<std::string> axiomes;
		std::map<char, std::vector<Rule<axShape> > >::iterator it1;
		std::map<char, std::vector<Rule<axLevel> > >::iterator it2;
		do
		{
			ligne = utils::noComment(fichier);
			if(!ligne.compare("[RULES]"))
				ligne = utils::noComment(fichier);
			if(!ligne.empty() && ligne.compare("[BUILDINGS]"))
			{
				champs = utils::split(ligne, ':');
				assert(champs.size() == 3);
				Rule<axShape> r;
				r.setSymbol(champs.at(0).at(0));
				axiomes = utils::split(champs.at(1), ',');
				std::vector<char> as;
				for(unsigned int i = 0; i < axiomes.size(); ++i)
				{
					as.push_back(axiomes.at(i).at(0));
				}
				r.setAxiomes(as);
				float p = std::stof(champs.at(2));
				p = (p < 0.0)? 0.0 : ((p > 1.0)? 1.0 : p);
				r.setProbability(p);
				it1 = m_blockRules.find(r.symbol());
				if(it1 == m_blockRules.end())
				{
					std::vector<Rule<axShape> > rules;
					rules.push_back(r);
					m_blockRules.insert(std::make_pair(r.symbol(), rules));
				}
				else
				{
					it1->second.push_back(r);
				}
			}
		}
		while(!ligne.empty() && ligne.compare("[BUILDINGS]"));
		assert(ligne.compare("[BUILDINGS]") == 0);
		do
		{
			ligne = utils::noComment(fichier);
			if(!ligne.empty() && ligne.compare("[ARCHI]"))
			{
				champs = utils::split(ligne, ':');
				Rule<axLevel> r;
				r.setSymbol(champs.at(0).at(0));
				axiomes = utils::split(champs.at(1), ',');
				axLevel t =  std::make_tuple('A', 0.,0.);
				std::get<0>(t) = axiomes.at(0).at(0);
				std::get<1>(t) = std::stof(axiomes.at(1));
				std::get<2>(t) = std::stof(axiomes.at(2));
				r.setAxiomes(t);
				float p = std::stof(champs.at(2));
				p = (p < 0.0)? 0.0 : ((p > 1.0)? 1.0 : p);
				r.setProbability(p);
				it2 = m_buildRules.find(r.symbol());
				if(it2 == m_buildRules.end())
				{
                    std::vector< Rule<axLevel> > rules;
					rules.push_back(r);
					m_buildRules.insert(std::make_pair(r.symbol(), rules));
				}
				else
				{
					it2->second.push_back(r);
				}

			}
		}
		while(ligne.compare("[ARCHI]"));
		do
		{
			ligne = utils::noComment(fichier);
			if(!ligne.empty())
			{
				champs = utils::split(ligne, ':');
				assert(champs.size() == 3);
				Rule<axArchi> r;
				r.setSymbol(champs.at(0).at(0));
				axiomes = utils::split(champs.at(1), ',');
				std::vector<char> as;
				for(unsigned int i = 0; i < axiomes.size(); ++i)
				{
					as.push_back(axiomes.at(i).at(0));
				}
				r.setAxiomes(as);
				float p = std::stof(champs.at(2));
				p = (p < 0.0)? 0.0 : ((p > 1.0)? 1.0 : p);
				r.setProbability(p);
				it1 = m_archiRules.find(r.symbol());
				if(it1 == m_archiRules.end())
				{
					std::vector<Rule<axArchi> > rules;
					rules.push_back(r);
					m_archiRules.insert(std::make_pair(r.symbol(), rules));
				}
				else
				{
					it1->second.push_back(r);
				}
			}
		}
		while(!ligne.empty());
		equalizeProbabilities();
	}
	fichier.close();
}

bool Grammar::checkRules(const char symbol)
{
	return !(m_blockRules.find(symbol) == m_blockRules.end());
}

template<typename T>
void Grammar::chooseClosestRule(const double &r, std::vector<Rule<T>> &rules, T &chosen)
{
	double min_diff = 2;
	double diff = 0;
	unsigned int c = 0;
	for(unsigned int i = 0; i < rules.size(); ++i)
	{
		diff = r - rules[i].probability();
		if(min_diff > diff)
		{
			min_diff = diff;
			c = i;
		}
	}
	chosen = rules.at(c).axiomes();
}

template<>
axShape Grammar::chooseRule(const char symbol, const bool _default)
{
	double r = RandomGen::getInstance().getDouble(.0, 1.);
	axShape chosen;
	std::vector<Rule<axShape> > rules = (_default)? m_blockRules.find(symbol)->second : m_archiRules.find(symbol)->second;

	for(std::vector<Rule<axShape> >::iterator it = rules.begin(); it != rules.end(); ++it)
	{
		if(r < it->probability())
		{
			chosen = it->axiomes();
			break;
		}
	}
	if(chosen.empty())
		chooseClosestRule<axShape>(r, rules, chosen);
	if(_default)
		std::sort(chosen.begin(), chosen.end(), utils::form::compare_form);
	return chosen;
}

template<>
axLevel Grammar::chooseRule(const char symbol, const bool _default)
{
	(void)_default;
	double r = RandomGen::getInstance().getDouble(0, 1);
	axLevel chosen = std::make_tuple('A', 0.0, 0.0);
	std::vector<Rule<axLevel> > rules = m_buildRules.find(symbol)->second;

	for(std::vector<Rule<axLevel> >::iterator it = rules.begin(); it != rules.end(); ++it)
	{
		if(r < it->probability())
		{
			chosen = it->axiomes();
			break;
		}
	}
	if(std::get<0>(chosen) == 'A')
		chooseClosestRule<axLevel>(r, rules, chosen);
	return chosen;
}

template<typename T>
void equalize(std::map<char, std::vector<Rule<T> > > &rule)
{
	double R = 0;
	for(auto it = rule.begin(); it != rule.end(); ++it)
	{
		for(auto it2 = it->second.begin(); it2 != it->second.end(); ++it2)
		{
			R += it2->probability();
		}
		if(R != 1.0)
		{
			for(auto it2 = it->second.begin(); it2 != it->second.end(); ++it2)
			{
				it2->setProbability(it2->probability()/R);
			}
		}
		R = 0;
	}

}

void Grammar::equalizeProbabilities()
{
	equalize<axShape>(m_blockRules);
	equalize<axLevel>(m_buildRules);
	equalize<axArchi>(m_archiRules);
}
