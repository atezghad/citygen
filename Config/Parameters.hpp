#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <map>
#include <string>

/**
 * @class Parameters
 * @brief Permet de contenir les paramètres définis par l'utilisateur
 */
class Parameters
{
	public:
		/**
		 * @brief Permet de récupérer l'instance statique
		 * @return Instance statique
		 */
		static Parameters& getInstance();

		/**
		 * @brief Permet d'initialiser les paramètres
		 * @param filename Chemin vers le fichier de paramètres
		 */
		static void initParameters(const std::string &filename);

		/**
		 * @brief Retourne un paramètre
		 * @param param Le paramètre
		 * @return paramètre
		 */
		static double get(const std::string &param);

	private:
		Parameters();
		static std::map<std::string,double> m_parameters;
};

#endif // PARAMETERS_H
