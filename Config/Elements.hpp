#ifndef ELEMENTS_H
#define ELEMENTS_H

#include <fstream>
#include <map>
#include <string>

#include <Architecture/ArchiElement.hpp>

/**
 * @class Elements
 * @brief Permet de contenir les différents éléménts architecturaux définis par l'utilisateur
 */
class Elements
{
	public:
		/**
		 * @brief Permet d'initialiser les paramètres
		 * @param filename Chemin vers le fichier de paramètres
		 */
		static Elements& getInstance();

		/**
		 * @brief Permet d'initialiser les éléments architecturaux
		 * @param filename Chemin vers le fichier des éléments architecturaux
		 */
		static void initElements(const std::string &filename);

		/**
		 * @brief Retourne un élément architectural
		 * @param param L'identifiant de l'élemenr
		 * @return Un élément architectural
		 */
		static ArchiElement* get(const int e);

		~Elements();
	private:
		Elements();
		static std::map<int, std::vector<ArchiElement* >> m_elements;

		Element initElement(std::ifstream &fichier);
		bool initWindows(const Element e, std::ifstream &fichier);
		bool initDoors(  const Element e, std::ifstream &fichier);
		bool initBalcons(const Element e, std::ifstream &fichier);
		bool initColumns(const Element e, std::ifstream &fichier);

};

#endif // ELEMENTS_H
