#ifndef GRAMMAR_H
#define GRAMMAR_H

#include <algorithm>
#include <cassert>
#include <map>
#include <string>
#include <tuple>
#include <typeinfo>
#include <vector>

#include <Config/Rule.hpp>
#include <Utils/utils.hpp>
#include <Utils/Random.hpp>

typedef std::vector<char>                axShape;
typedef std::vector<char>                axArchi;
typedef std::tuple<char, float, float>   axLevel;

/**
 * @class Grammar
 * @brief Permet de contenir les différentes règles de productions définies par l'utilisateur
 */
class Grammar
{
	public:
		/**
		 * @brief Permet de récupérer l'instance statique
		 * @return Instance statique
		 */
		static Grammar& getInstance();

		/**
		 * @brief Permet d'initialiser la grammaire
		 * @param filename Chemin vers le fichier de grammaire
		 */
		void initGrammar(const std::string &filename);


		template<typename T>
		/**
		 * @brief Permet de choisir une règle aléatoirement
		 * @param symbol Le symbole de la grammaire
		 * @param _default true si comparaison des symboles, false sinon
		 * @return La règle choisie
		 */
		T chooseRule(const char symbol, const bool _default=true);

		/**
		 * @brief Vérifie si des règles sont définies
		 * @param symbol Le symbole
		 * @return True si des règles existent pour symbol, false sinon
		 */
		bool checkRules(const char symbol);

	private:
		Grammar();
		std::map<char, std::vector<Rule<axShape> > > m_blockRules;
		std::map<char, std::vector<Rule<axLevel> > > m_buildRules;
		std::map<char, std::vector<Rule<axArchi> > > m_archiRules;

		template<typename T>
		static void chooseClosestRule(const double &r, std::vector<Rule<T>> &rules, T &chosen);

		void equalizeProbabilities();

};

#endif // GRAMMAR_H
