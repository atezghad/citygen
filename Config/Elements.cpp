#include <iostream>
#include <fstream>
#include <cassert>
#include <cmath>

#include <Config/Elements.hpp>
#include <Architecture/Door.hpp>
#include <Architecture/Balcon.hpp>
#include <Architecture/Window.hpp>
#include <Architecture/Column.hpp>
#include <Architecture/archi.hpp>
#include <Utils/utils.hpp>
#include <Utils/Random.hpp>
#include <Utils/structs.hpp>

std::map<int, std::vector<ArchiElement*> > Elements::m_elements;

Element Elements::initElement(std::ifstream &fichier)
{
	std::string ligne = utils::noComment(fichier);
	std::vector<std::string> champs;
	Element e;
	Volume v;
	int i = 0;
	if(!ligne.compare("*"))
	{
		ligne = utils::noComment(fichier);
		do
		{
			if(!ligne.empty())
			{
				champs = utils::split(ligne, ':');
				assert(champs.size() == 2);
				if(champs.at(0) == "WIDTH_SPACE")
					e.widthSpace = std::stod(champs.at(1));
				else if(champs.at(0) == "LENGTH_SPACE")
					e.lengthSpace = std::stod(champs.at(1));
				else if(champs.at(0) == "THICKNESS")
					e.thickness = std::stod(champs.at(1));
				else if(champs.at(0) == "LENGTH")
					v.l = std::stod(champs.at(1));
				else if(champs.at(0) == "WIDTH")
					v.L = std::stod(champs.at(1));
				else if(champs.at(0) == "HEIGHT")
					v.h = std::stod(champs.at(1));
				++i;
				if(i < 6)
					ligne = utils::noComment(fichier);
			}
		}
		while(i != 6);
	}
	e.volume = v;
	return e;

}

std::map<std::string, double> getValues(std::initializer_list<std::string> l, std::ifstream &fichier, bool &b)
{
	std::string ligne = utils::noComment(fichier);
	std::vector<std::string> champs;
	std::map<std::string, double> values;
	for(unsigned int i = 0; i < l.size(); ++i)
	{
		champs = utils::split(ligne, ':');
		assert(champs.size() == 2);
		for(std::initializer_list<std::string>::iterator it = l.begin(); it != l.end(); ++it)
		{
			if(champs.at(0) == *it)
			{
				values.insert(std::make_pair(champs.at(0), std::stod(champs.at(1))));
				break;
			}
		}
		if(i != l.size()-1)
			ligne = utils::noComment(fichier);
	}
	b = (ligne.compare("*"))? false : true;
	return values;
}


bool Elements::initDoors(const Element e, std::ifstream &fichier)
{
	bool res;
	std::map<std::string, double> values = getValues({"HANDLE_X", "HANDLE_Y", "HANDLE_Z"}, fichier, res);
	Point3 poignee(values.at("HANDLE_X"), values.at("HANDLE_Y"), values.at("HANDLE_Z"));
	auto it = m_elements.find(archi::DOOR);
	if(it == m_elements.end())
	{
		std::vector<ArchiElement*> m;
		m.push_back(new Door(e, poignee));
		m_elements.insert(std::make_pair(archi::DOOR, m));
	}
	else
	{
		it->second.push_back(new Door(e, poignee));
	}
	return res;
}


bool Elements::initWindows(const Element e, std::ifstream &fichier)
{
	bool res;
	std::map<std::string, double> values = getValues({"LATTICE_H", "LATTICE_V"}, fichier, res);
	auto it = m_elements.find(archi::WINDOW);
	if(it == m_elements.end())
	{
		std::vector<ArchiElement*> m;
		m.push_back(new Window(e, values.at("LATTICE_H"), values.at("LATTICE_V")));
		m_elements.insert(std::make_pair(archi::WINDOW, m));
	}
	else
	{
		it->second.push_back(new Window(e, values.at("LATTICE_H"), values.at("LATTICE_V")));
	}
	return res;
}

bool Elements::initBalcons(const Element e, std::ifstream &fichier)
{
	bool res;
	std::map<std::string, double> values = getValues({"BAR_H", "BAR_V"}, fichier, res);
	auto it = m_elements.find(archi::BALCON);
	if(it == m_elements.end())
	{
		std::vector<ArchiElement*> m;
		m.push_back(new Balcon(e, values.at("BAR_H"), values.at("BAR_V")));
		m_elements.insert(std::make_pair(archi::BALCON, m));
	}
	else
	{
		it->second.push_back(new Balcon(e, values.at("BAR_H"), values.at("BAR_V")));
	}
	return res;
}

bool Elements::initColumns(const Element e, std::ifstream &fichier)
{
	bool res;
	std::string ligne = utils::noComment(fichier);
	res = (ligne.compare("*"))? false : true;
	auto it = m_elements.find(archi::COLUMN);
	if(it == m_elements.end())
	{
		std::vector<ArchiElement*> m;
		m.push_back(new Column(e));
		m_elements.insert(std::make_pair(archi::COLUMN, m));
	}
	else
	{
		it->second.push_back(new Column(e));
	}
	return res;
}

void Elements::initElements(const std::string &filename)
{
	std::ifstream fichier;
	std::string ligne;
	fichier.open(filename.c_str());
	bool keep_reading = true;
	if(fichier.is_open())
	{
		do
		{
			ligne = utils::noComment(fichier);
			if(!ligne.compare("[DOORS]"))
			{
				while(keep_reading && !ligne.empty())
				{
					Element e = Elements::getInstance().initElement(fichier);
					keep_reading = Elements::getInstance().initDoors(e, fichier);
				}
				keep_reading = true;
			}
			if(!ligne.compare("[WINDOWS]"))
			{
				while(keep_reading && !ligne.empty())
				{
					Element e = Elements::getInstance().initElement(fichier);
					keep_reading = Elements::getInstance().initWindows(e, fichier);
				}
				keep_reading = true;
			}
			if(!ligne.compare("[BALCONS]"))
			{
				while(keep_reading && !ligne.empty())
				{
					Element e = Elements::getInstance().initElement(fichier);
					keep_reading = Elements::getInstance().initBalcons(e, fichier);
				}
				keep_reading = true;
			}
			if(!ligne.compare("[COLUMNS]"))
			{
				while(keep_reading && !ligne.empty())
				{
					Element e = Elements::getInstance().initElement(fichier);
					keep_reading = Elements::getInstance().initColumns(e, fichier);
				}
				keep_reading = true;
			}

		}
		while(!ligne.empty());
		fichier.close();
	}
	else
	{
		std::cerr << "Fichier des dimensions des éléments manquant!!" << std::endl;
		exit(EXIT_FAILURE);
	}
}

Elements::Elements()
{}

Elements& Elements::getInstance()
{
	static Elements instance;
	return instance;
}

ArchiElement* Elements::get(const int e)
{
	std::map<int, std::vector<ArchiElement*>>::iterator p = Elements::getInstance().m_elements.find(e);
	if(p == m_elements.end())
	{
		std::cerr << "Element introuvable, renvoie nullptr!!" << std::endl;
		return nullptr;
	}
	else
	{
		return p->second.at(RandomGen::getInstance().getInt(0, p->second.size()-1));
	}
}

Elements::~Elements()
{
	for(auto it = m_elements.begin(); it != m_elements.end(); ++it)
	{
		for(auto it1 = it->second.begin(); it1 != it->second.end(); ++it1)
		{
			delete *it1;
		}
	}
}
