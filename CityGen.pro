TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
QMAKE_CXXFLAGS += -O3

SOURCES += main.cpp \
    Shapes/Quad.cpp \
    Shapes/Triangle.cpp \
    Utils/Random.cpp \
    Utils/utils.cpp \
    Utils/subdiv.cpp \
    Mesh/Mesh.cpp \
    Building/Level.cpp \
    Building/Ground.cpp \
    Building/Floor.cpp \
    Building/Roof.cpp \
    Config/Parameters.cpp \
    Shapes/Cube.cpp \
    Utils/Geometry/Point2.cpp \
    Utils/Geometry/Point3.cpp \
    Utils/Geometry/Vec2.cpp \
    Utils/Geometry/Vec3.cpp \
    Shapes/Prism.cpp \
    Config/Grammar.cpp \
    Shapes/Shape2.cpp \
    Shapes/Shape3.cpp \
    Building/Building.cpp \
    Utils/Geometry/Plane.cpp \
    Architecture/archi.cpp \
    Architecture/ArchiElement.cpp \
    Architecture/Window.cpp \
    Architecture/Door.cpp \
    Architecture/Balcon.cpp \
    Config/Elements.cpp \
    Architecture/Column.cpp

HEADERS += \
    Shapes/Quad.hpp \
    Shapes/Triangle.hpp \
    Utils/Random.hpp \
    Utils/utils.hpp \
    Utils/subdiv.hpp \
    Mesh/Mesh.hpp \
    Building/Level.hpp \
    Building/Ground.hpp \
    Building/Floor.hpp \
    Building/Roof.hpp \
    Config/Parameters.hpp \
    Shapes/Cube.hpp \
    Utils/Geometry/Point2.hpp \
    Utils/Geometry/Point3.hpp \
    Utils/Geometry/Vec2.hpp \
    Utils/Geometry/Vec3.hpp \
    Shapes/Prism.hpp \
    Config/Grammar.hpp \
    Shapes/Shape2.hpp \
    Shapes/Shape3.hpp \
    Building/Building.hpp \
    Utils/structs.hpp \
    Utils/Geometry/Plane.hpp \
    Architecture/archi.hpp \
    Architecture/ArchiElement.hpp \
    Architecture/Window.hpp \
    Architecture/Door.hpp \
    Architecture/Balcon.hpp \
    Config/Elements.hpp \
    Config/Rule.hpp \
    Architecture/Column.hpp
