#ifndef MAILLAGE_H
#define MAILLAGE_H

#include <list>
#include <string>


#include <Shapes/Shape2.hpp>
#include <Building/Building.hpp>
#include <Shapes/Cube.hpp>
#include <Shapes/Prism.hpp>
#include <Utils/Geometry/Point2.hpp>

#define TRIANGLE    3
#define QUAD        4

/**
 * \class Mesh
 * \brief Cette classe permet de contenir toutes les formes générées par la grammaire
 * */
class Mesh final
{
	public:
		/**
		 * @brief Mesh
		 * @param a Le point d'origine
		 * @param b Le point diagonalement opposé à b
		 * @param rule_filename Chemin vers le fichier contenant la grammaire
		 * @param param_filename Chemin vers le fichiers contenant les paramètres
		 * @param elem_filename	 Chemin vers le fichiers contenant les dimensions des éléments architecturaux
		 */
		Mesh(Point2 &a, Point2 &b,
			 const std::string &rule_filename,
			 const std::string &param_filename, const std::string &elem_filename);

		/**
		 * @brief Mesh avec paramètres par défaut
		 * @param a Le point d'origine
		 * @param b Le point diagonalement opposé à b
		 * @param rule_filename Chemin vers le fichier contenant la grammaire
		 * @param elem_filename	 Chemin vers le fichiers contenant les dimensions des éléments architecturaux
		 */
		Mesh(Point2 &a, Point2 &b,
			 const std::string &rule_filename,
			 const std::string &elem_filename);

		/**
		 * @brief Permet de générer la ville selon les paramètres
		 */
		void                 generateCity();

		/**
		 * @brief Permet d'exporter les quartiers en OBJ
		 * @param destination
		 */
		void                 blocksToOBJ(const std::string destination);

		/**
		 * @brief  Permet d'exporter les bâtiments en OBJ
		 * @param destination
		 */
		void                 buildingsToOBJ(const std::string destination);

		~Mesh();

	private:
		std::list<Shape2*>     m_forms;
		std::list<Shape2*>     m_subforms;
		std::list<Building*>   m_buildings;
        std::list<Shape3*>     m_levels;
		Point2                 origin;
		Point2                 arrival;

		void                 formToBlock();
		void                 generateBuildings();
		void                 generateNetwork();
		void                 generateSubforms(const Shape2* base, const Shape2 *n);
        void                 generateRoads();
		void                 generateArchElements();

        template<typename T>
        void                    clearList(std::list<T> &l)
        {
            if(std::is_pointer<T>::value)
            {
                for(auto it = l.begin(); it != l.end(); ++it)
                {
                    delete *it;
                }
            }
        }

};

#endif // MAILLAGE_H
