#define _USE_MATH_DEFINES
#include <iostream>
#include <fstream>
#include <cmath>

#include <Mesh/Mesh.hpp>
#include <Shapes/Quad.hpp>
#include <Config/Grammar.hpp>
#include <Building/Ground.hpp>
#include <Shapes/Triangle.hpp>
#include <Utils/Geometry/Vec2.hpp>
#include <Config/Parameters.hpp>
#include <Config/Elements.hpp>


Mesh::Mesh(Point2 &a, Point2 &b, const std::string& rule_filename, const std::string &param_filename, const std::string &elem_filename) : origin(a), arrival(b)
{
	Parameters::getInstance().initParameters(param_filename);
	Grammar::getInstance().initGrammar(rule_filename);
	Elements::getInstance().initElements(elem_filename);
}

Mesh::Mesh(Point2 &a, Point2 &b, const std::string& rule_filename, const std::string &elem_filename) : origin(a), arrival(b)
{
	Parameters::getInstance().initParameters("");
	Grammar::getInstance().initGrammar(rule_filename);
	Elements::getInstance().initElements(elem_filename);
}

void Mesh::generateCity()
{
	std::cout << "==== Génération de la ville en cours ...==== " << std::endl;
	std::cout << "** Découpage de la zone en cours ...";
	generateNetwork();
	std::cout << " Terminé!" << std::endl;
	std::cout << "** Création des routes en cours ...";
	generateRoads();
	std::cout << " Terminé!" << std::endl;
	std::cout << "** Création des trottoirs en cours ...";
	formToBlock();
	std::cout << " Terminé!" << std::endl;
	std::cout << "** Création des batiments en cours ...";
	generateBuildings();
	std::cout << " Terminé!" << std::endl;
	std::cout << "** Création des éléments architecturaux en cours ...";
	generateArchElements();
	std::cout << " Terminé!" << std::endl;
	std::cout << "==== Génération de la ville terminée !  ==== " << std::endl;
}

void Mesh::generateNetwork()
{
	Vec2 _a(origin, arrival);
	Point2 c1(_a.x(), 0.);
	Point2 c2(0., _a.y());
	Point2 u(origin + c1);
	Point2 v(origin + c2);

	m_forms.push_back(new Quad(origin, u,  arrival, v));
	m_forms.front()->subdivide(m_forms);
}

void Mesh::generateRoads()
{
	for(std::list<Shape2*>::iterator it = m_forms.begin(); it != m_forms.end(); ++it)
		(*it)->shrink(Parameters::getInstance().get("ROAD_WIDTH"));
}

void Mesh::formToBlock()
{
	for(std::list<Shape2*>::iterator base = m_forms.begin(); base != m_forms.end(); ++base)
	{
		if((*base)->points().size() == QUAD)
		{
			Quad b = *((Quad*)(*base));
			Quad q = b;
			if(q.shrink(Parameters::getInstance().get("PAVEMENT_WIDTH")))
			{
				m_subforms.push_back(new Quad(q));
				m_subforms.back()->subdivide(m_subforms, false);
			}
		}
		else if((*base)->points().size() == TRIANGLE)
		{
			Triangle b = *((Triangle*)(*base));
			Triangle t = b;
			if(t.shrink(Parameters::getInstance().get("PAVEMENT_WIDTH")))
			{
				m_subforms.push_back(new Triangle(t));
				m_subforms.back()->subdivide(m_subforms, false);
			}
		}
	}
	for(std::list<Shape2*>::iterator base = m_subforms.begin(); base != m_subforms.end(); ++base)
	{
		(*base)->shrink(Parameters::getInstance().get("SAFETY"));
	}
}

void Mesh::generateBuildings()
{
	for(std::list<Shape2*>::iterator it = m_subforms.begin(); it != m_subforms.end(); ++it)
	{
		m_buildings.push_back(new Building(*it));
	}
}
void Mesh::generateArchElements()
{
    for(std::list<Building*>::iterator it = m_buildings.begin(); it != m_buildings.end(); ++it)
        (*it)->generateArchElements(m_levels);
}

void Mesh::blocksToOBJ(const std::string destination)
{
	std::cout << "==== Export OBJ de la ville en cours ...==== " << std::endl;
	std::cout << "** Export des rues en cours ...";
	std::ofstream obj;
	obj.open(destination.c_str(), std::ios::out | std::ios::trunc);
	for(std::list<Shape2*>::iterator it = m_forms.begin(); it != m_forms.end(); ++it)
	{
		(*it)->exportVertices(obj);
	}
	unsigned int v = 1;
	for(std::list<Shape2*>::iterator it = m_forms.begin(); it != m_forms.end(); ++it)
	{
		(*it)->exportFaces(v, obj);
	}
	obj.close();
	std::cout << " Terminé!" << std::endl;
}

void Mesh::buildingsToOBJ(const std::string destination)
{
	std::cout << "==== Export OBJ  en cours ...==== " << std::endl;
	std::cout << "** Export des bâtiments en cours ...";
	std::ofstream obj;
	unsigned int v = 0;
	obj.open(destination.c_str(), std::ios::in | std::ios::trunc);

	for(std::list<Building*>::iterator it = m_buildings.begin(); it != m_buildings.end(); ++it)
	{
		(*it)->exportVertices(obj);
	}
    for(std::list<Shape3*>::iterator it = m_levels.begin(); it != m_levels.end(); ++it)
    {
        (*it)->exportVertices(obj);
    }
	for(std::list<Building*>::iterator it = m_buildings.begin(); it != m_buildings.end(); ++it)
	{
		(*it)->exportFaces(v, obj);
	}
    for(std::list<Shape3*>::iterator it = m_levels.begin(); it != m_levels.end(); ++it)
	{
        (*it)->exportFaces(v,obj);
	}
	obj.close();
	std::cout << " Terminé!" << std::endl;
}

Mesh::~Mesh()
{
    clearList<Shape2*>(m_forms);
    clearList<Shape2*>(m_subforms);
    clearList<Shape3*>(m_levels);
    clearList<Building*>(m_buildings);
}

