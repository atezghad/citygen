#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <Shapes/Shape2.hpp>
#include <Utils/subdiv.hpp>

/**
 * \class Triangle
 * \brief Cette classe permet de représenter un triangle
 * */
class Triangle final: public Shape2
{
	public:
		Triangle();

		/**
		 * @brief Triangle
		 * @param a Point du triangle
		 * @param b	Point du triangle
		 * @param c	Point du triangle
		 */
		Triangle(const Point2& a, const Point2& b, const Point2& c);
		Triangle(const Triangle &t);
		Triangle(Shape2 *t);
		Triangle(Triangle &&t);
		~Triangle();

		Triangle& operator =(const Triangle &t);
		bool operator==(const Triangle &t) const;
		bool operator!=(const Triangle &t) const;

		double area() const override;
		double shapeIndex() const override;
		double idealShapeIndex() const override;
		bool checkRules() const override;
		axShape chooseRule() override;
		Point2 randomPoint() override;
		void setCenter() override;
		std::list<Shape2*> specificSubdivide(const axShape &chosen, const bool &road=true) override;
        bool inside(const Point2 &p) const override;

		/**
		 * @brief Retourne le symbole représentant la forme Quad
		 * @return Le symbole représentant la forme Quad
		 */
		static char symbol();

	private:
		static char _symbol;

		bool validForm(const std::vector<Point2> &v) override;

};

#endif // TRIANGLE_H
