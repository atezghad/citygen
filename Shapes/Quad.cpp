#define _USE_MATH_DEFINES
#include <fstream>
#include <cassert>
#include <algorithm>

#include <Shapes/Quad.hpp>
#include <Shapes/Triangle.hpp>
#include <Utils/Geometry/Vec2.hpp>
#include <Utils/Random.hpp>
#include <Utils/utils.hpp>
#include <Utils/subdiv.hpp>

char Quad::_symbol = 'Q';

Quad::Quad()
{}

Quad::Quad(const Point2 &a, const Point2 &b, const Point2 &c, const Point2 &d)
{
    m_points.insert(m_points.end(), { a, b, c, d });
	setCenter();
	setNormals();
}

Quad::Quad(const Quad &t)
{
	m_points = t.m_points;
	m_normals = t.m_normals;
	m_center = t.m_center;
}

Quad::Quad(Shape2 *t)
{
	std::vector<Point2> p = t->points();
	if(p.size() == 4)
	{
		m_points = p;
		m_normals = t->normals();
		m_center = t->center();
	}
}

Quad::Quad(Quad &&t)
{
	m_points = t.m_points;
	m_normals = t.m_normals;
	m_center = t.m_center;
	t.m_points.clear();
	t.m_normals.clear();
}

Quad::~Quad()
{
	m_points.clear();
	m_normals.clear();
}

Quad& Quad::operator =(Quad &t)
{
	m_points = t.m_points;
	m_normals = t.m_normals;
	m_center = t.m_center;
	return *this;
}

bool Quad::operator ==(Quad &t)
{
	return (m_points[0] == t.m_points[0]
			&& m_points[1] == t.m_points[1]
			&& m_points[2] == t.m_points[2]
			&& m_points[3] == t.m_points[3]
			);
}

bool Quad::operator !=(Quad &t)
{
	return (!(*this == t ));
}

double Quad::area() const
{
	Triangle a(m_points[0], m_points[2], m_points[3]);
	Triangle b(m_points[0], m_points[1], m_points[2]);
	return a.area() + b.area();
}

bool Quad::validForm(const std::vector<Point2> &v)
{
	if(utils::geom::intersectSegments2D(v[0], v[1], v[2], v[3]))
		return false;
	return true;
}

axShape Quad::chooseRule()
{
	return Grammar::getInstance().chooseRule<axShape>(_symbol);
}

void Quad::setCenter()
{
	Triangle a(m_points[0], m_points[2], m_points[3]);
	Triangle b(m_points[0], m_points[1], m_points[2]);
	Triangle c(m_points[0], m_points[1], m_points[3]);
	Triangle d(m_points[1], m_points[2], m_points[3]);
	m_center = (a.center()+b.center()+c.center()+d.center())/4.0;
}

std::list<Shape2*> Quad::specificSubdivide(const axShape &chosen, const bool &road)
{
	Quad *remnant_q = this;
	Triangle * remnant_t = nullptr;
	bool subdividedToTriangle = false;
	bool over = true;
	std::list<Shape2*> subdivs;

	for(axShape::const_iterator r = chosen.begin(); r != chosen.end(); ++r)
	{
		if((remnant_q->canSubdivide(road)) || (subdividedToTriangle && remnant_t->canSubdivide(road)))
		{
			if(*r == _symbol)
				subdiv::quadToQuad(remnant_q, subdivs);
			else if(*r == Triangle::symbol())
			{
				if(!subdividedToTriangle)
				{
					remnant_t = (Triangle*)subdiv::quadToTriangle(remnant_q, subdivs);
					subdividedToTriangle = true;
				}
				else
					subdiv::triangleToTriangle(remnant_t, subdivs);
			}
		}
		else
		{
			subdivs.push_back((subdividedToTriangle)? (Shape2*)remnant_t : (Shape2*)new Quad(remnant_q));
			over = false;
			break;
		}
	}
	if(over)
		subdivs.push_back((subdividedToTriangle)? (Shape2*)remnant_t : (Shape2*)new Quad(remnant_q));

    return subdivs;
}

bool Quad::inside(const Point2 &p) const
{
    Triangle a(m_points[0], m_points[2], m_points[3]);
    Triangle b(m_points[0], m_points[1], m_points[2]);
    Triangle c(m_points[0], m_points[1], m_points[3]);
    Triangle d(m_points[1], m_points[2], m_points[3]);

    return a.inside(p) || b.inside(p) || c.inside(p) || d.inside(p);
}

char Quad::symbol()
{
	return _symbol;
}

bool Quad::checkRules() const
{
	return Grammar::getInstance().checkRules(_symbol);
}

double Quad::shapeIndex() const
{
	return perimeter()/(2.0*std::sqrt(M_PI*area()));
}

double Quad::idealShapeIndex() const
{
	return Parameters::getInstance().get("QUAD_INDEX");
}

Point2 Quad::randomPoint()
{
	double u = RandomGen::getInstance().getDouble(.45, .55);
	double v = RandomGen::getInstance().getDouble(.45, .55);
	return Point2(m_points[0] + u*Vec2(m_points[0], m_points[1]) + v*Vec2(m_points[0], m_points[3]));
}
