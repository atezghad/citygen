#ifndef CUBE_H
#define CUBE_H

#include <Shapes/Shape3.hpp>

/**
 * \class Cube
 * @brief Permet de représenter un pavé droit
 */
class Cube: public Shape3
{
	public:
		/**
		 * @brief Constructeur
		 * @param o Point d'origine
		 * @param l Longueur
		 * @param L Largeur
		 * @param h Hauteur
		 * @param normal Vecteur normal au plan
		 */
		Cube(const Point3 &o, const double &l, const double &L, const double &h, const Vec3 &normal);

		/**
		 * @brief Cube
		 * @param a Point de base
		 * @param b	Point de base
		 * @param c	Point de base
		 * @param d	Point de base
		 * @param l Longeur
		 * @param normal Vecteur normal au plan
		 */
		Cube(const Point3 &a, const Point3 &b, const Point3 &c, const Point3 &d, const double &l,  const Vec3 &normal);


		std::vector<indexes> toTriangles() override;

		/**
		 * @brief Retourne l'ordre des points pour générations des triangles
		 * @return Ordre des points pour générations des triangles
		 */
		static std::vector<indexes> getIndexes();


        ~Cube() override;

	private:
		static std::vector<indexes> pts;
		double m_l;
		double m_L;
		double m_h;

		void createCube();
		void create(const Point3 &o, const Vec3 &normal) override;

};
#endif // CUBE_H
