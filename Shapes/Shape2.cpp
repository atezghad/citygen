#include <algorithm>
#include <limits>
#include <fstream>
#include <Shapes/Shape2.hpp>


Point2 Shape2::at(const unsigned int &i) const
{
	return m_points.at(i);
}

std::vector<Point2> Shape2::points() const
{
	return m_points;
}

void Shape2::set(const Point2 &c, const unsigned int &i)
{
	m_points.at(i) = c;
	setCenter();
	setNormals();
}

void Shape2::setNormals()
{
	m_normals.clear();
	unsigned int nbpoints = m_points.size();
	for(unsigned int i = 0; i < nbpoints; ++i)
		m_normals.push_back(Vec2(m_points[i], m_center).normalize());
}

bool Shape2::shrink(const double &d)
{
	if(minDistToCenter() > d)
	{
		std::vector<Point2> _new;
		unsigned int nbpoints = m_points.size();
		for(unsigned int i = 0; i < nbpoints; ++i)
			_new.push_back(m_points[i] + d * m_normals[i]);
		if(validForm(_new))
		{
			for(unsigned int i = 0; i < nbpoints; ++i)
				m_points[i] = _new[i];
			return true;
		}
		else return false;
	}
	else return false;
}

double Shape2::perimeter() const
{
	double p = .0;
	unsigned int nbpoints = m_points.size();

	for(unsigned int i = 0; i <nbpoints; ++i)
		p += m_points[i].distance(m_points[(i+1) % nbpoints]);

	return p;
}

bool Shape2::canSubdivide(const bool &road) const
{
	if(road)
		return shapeIndex() <= idealShapeIndex() && area() >= Parameters::getInstance().get("MIN_AREA");
	else
		return shapeIndex() <= idealShapeIndex() + Parameters::get("DELTA_INDEX")
				&& shapeIndex() >= idealShapeIndex() - Parameters::get("DELTA_INDEX")
				&& area() >= Parameters::getInstance().get("MIN_SUBAREA");
}

double Shape2::minDistToCenter() const
{
	double min = std::numeric_limits<double>::max(), dist = 0;
	unsigned int nbpoints = m_points.size();
	for(unsigned int i = 0; i < nbpoints; ++i)
	{
		dist = m_points[i].distance(m_center);
		if(dist < min)
			min = dist;
	}
	return min;
}

void Shape2::subdivide(std::list<Shape2*> &m, const bool &road)
{
	if(canSubdivide(road) && checkRules())
	{
		std::list<Shape2*> subdivs = specificSubdivide(chooseRule(), road);
		for(std::list<Shape2 *>::iterator it = subdivs.begin(); it != subdivs.end(); ++it)
		{
			m.push_back(*it);
			(*it)->subdivide(m, road);
		}
		std::list<Shape2 *>::iterator me = std::find(m.begin(), m.end(), this);
		m.erase(me);
	}
}

Point2 Shape2::center() const
{
	return m_center;
}

std::vector<Vec2> Shape2::normals() const
{
	return m_normals;
}

void Shape2::exportVertices(std::ofstream &obj)
{
	for(unsigned int i = 0; i < m_points.size(); ++i)
	{
		obj << "v " << m_points[i].x() << " " << 0 << " " << m_points[i].y() << std::endl;
	}
}

void Shape2::exportFaces(unsigned int &index, std::ofstream &obj)
{
	unsigned int s = m_points.size();
	switch(s)
	{
		case 3:
		{
			obj << "f " << index << " " << index+2 << " " << index+1 << std::endl;
			break;
		}

		case 4:
		{
			obj << "f " << index   << " " << index+3 << " " << index+1 << std::endl;
			obj << "f " << index+1 << " " << index+3 << " " << index+2 << std::endl;
			break;
		}
	}
	index+=s;
}
