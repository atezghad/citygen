/**
* \file Form.hpp
* \author Anis Tezghadanti
* */
#ifndef FORME_H
#define FORME_H

#include <vector>
#include <list>
#include <utility>

#include <Config/Grammar.hpp>
#include <Config/Parameters.hpp>
#include <Utils/Geometry/Vec2.hpp>


/**
 * \class Shape2
 * \brief Cette classe permet de représenter une forme géométrique de manière générique
 * */
class Shape2
{
	public:
		/**
		 * @brief area
		 * @return Retourne l'aire de la forme
		 */
		virtual double area() const = 0;

		/**
		 * @brief Vérifie si des règles sont définies pour la forme
		 * @return true si règles existantes, false sinon
		 */
		virtual bool checkRules() const = 0;

		/**
		 * @brief Permet de choisir une des règles définies
		 * @return La règle choisie pour la subdivision
		 */
		virtual axShape chooseRule() = 0;

		/**
		 * @brief Permet de calculer le centroïde de la forme
		 */
		virtual void setCenter() = 0;

		/**
		 * @brief Retourne un indice de la forme à partir de son aire et de son périmètre
		 * @return Indice
		 */
		virtual double shapeIndex() const = 0;

		/**
		 * @brief Retourne la valeur pour l'indice de cette forme idéale (Triangle rectangle pour un triangle, carré pour un Quad...)
		 * @return Indice idéal
		 */
		virtual double idealShapeIndex() const = 0;

		/**
		 * @brief Réalise une subdivision spécifique à la forme
		 * @param chosen La règle choisie pour la subdivision
		 * @param road True si subdivison en quartiers, false sinon
		 * @return Une liste des formes générées par la subdivision
		 */
		virtual std::list<Shape2*> specificSubdivide(const axShape &chosen, const bool &road=true) = 0;

		/**
		 * @brief Retourne un point aléatoire sur la forme
		 * @return Point aléatoire
		 */
		virtual Point2 randomPoint() = 0;

		virtual ~Shape2(){}

		/**
		 * @brief Retourne le i-ème point de la forme
		 * @param i L'indice
		 * @return Le i-ème point
		 */
		Point2 at(const unsigned int &i) const;

		/**
		 * @brief Permet d'exporter les points de la forme
		 * @param obj Le fichier
		 */
		void exportVertices(std::ofstream &obj);

		/**
		 * @brief  Permet d'exporter les faces de la forme
		 * @param[in][out] index La valeur de l'indice actuel
		 * @param[in][out] obj Le fichier
		 */
		void exportFaces(unsigned int &index, std::ofstream &obj) ;

		/**
		 * @brief Vérifie si la forme peut se subdiviser selon son aire, son indice, et l'existence de ses règles
		 * @param road True si subdivison en quartiers, false sinon
		 * @return True si subdivison possible, false sinon
		 */
		bool canSubdivide(const bool &road=true) const;

		/**
		 * @brief Retourne la plus petite distance au centroïde
		 * @return La plus petite distance au centroïde
		 */
		double minDistToCenter() const;

		/**
		 * @brief Retourne le perimètre
		 * @return Perimetre
		 */
		double perimeter() const;

		/**
		 * @brief Retourne l'enssemble des points
		 * @return Les points
		 */
		std::vector<Point2> points() const;

		/**
		 * @brief Permet de remplacer le i-ème Point
		 * @param c Le nouveau point
		 * @param i L'indice du point à remplacer
		 */
		void set(const Point2 &c, const unsigned int &i);

		/**
		 * @brief Permet de calculer les normales des points
		 */
		void setNormals();

		/**
		 * @brief Permet de rétrécir la forme
		 * @param d La distance à parcourir pour rétrécir la forme
		 * @return True si rétrécissement éffectué, false sinon
		 */
		bool shrink(const double& d);

		/**
		 * @brief Permet de subdiviser la forme
		 * @param m Le conteneur de formes
		 * @param road True si subdivison en quartiers, false sinon
		 */
		void subdivide(std::list<Shape2*> &m, const bool &road=true);

		/**
		 * @brief Renvoie le centroïde de la forme
		 * @return le centroïde de la forme
		 */
		Point2 center() const;

		/**
		 * @brief Renvoie les normales des points
		 * @return Les normales des points
		 */
		std::vector<Vec2> normals() const;

        /**
          @brief
          @return
          */
        virtual bool inside(const Point2  &p) const = 0;

	protected:
		std::vector<Point2> m_points;
		std::vector<Vec2>   m_normals;
		Point2              m_center;

		virtual bool validForm(const std::vector<Point2> &v) = 0;
};


#endif // FORME_H
