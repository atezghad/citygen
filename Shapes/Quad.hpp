#ifndef QUAD_H
#define QUAD_H

#include <Config/Grammar.hpp>
#include <Shapes/Shape2.hpp>
#include <Utils/subdiv.hpp>

/**
 * \class Quad
 * \brief Cette classe permet de représenter un quadrilatère
 * */
class Quad final: public Shape2
{
	public:

		Quad();

		/**
		 * @brief Quad
		 * @param a Point du Quad
		 * @param b	Point du Quad
		 * @param c	Point du Quad
		 * @param d	Point du Quad
		 */
		Quad(const Point2 &a, const Point2 &b, const Point2 &c, const Point2 &d);

		Quad(const Quad &t);
		Quad(Quad &&t);
		Quad(Shape2 *t);
		~Quad();

		Quad& operator =(Quad &t);
		bool operator==(Quad &t);
		bool operator!=(Quad &t);

		double area() const override;
		bool checkRules() const override;
		axShape chooseRule() override;
		double shapeIndex() const override;
		double idealShapeIndex() const override;
		void setCenter() override;
		Point2 randomPoint() override;
		std::list<Shape2*> specificSubdivide(const axShape &chosen, const bool &road=true) override;
        bool inside(const Point2 &p) const override;

		/**
		 * @brief Retourne le symbole représentant la forme Quad
		 * @return Le symbole représentant la forme Quad
		 */
		static char symbol();

	private:
		static char _symbol;

		bool validForm(const std::vector<Point2> &v) override;
};

#endif // QUAD_H
