#include <algorithm>

#include <Shapes/Cube.hpp>
#include <Utils/utils.hpp>

std::vector<indexes> Cube::pts   =    {{0,2,1},{0,3,2},
									  {1,6,2},{1,5,6},
									  {3,0,4},{3,4,7},
									  {4,5,6},{4,6,7},
									  {0,1,5},{0,5,4},
									  {2,3,7},{2,7,6}};

void Cube::create(const Point3 &o, const Vec3 &normal)
{
	axisAngle a = getAxisAngle(Vec3::Z, normal);
	rotate(a.axis, a.angle);
	move(Vec3(o));
}

void Cube::createCube()
{
    m_points.insert(m_points.end(),
                    {
                        Point3(),
                        Point3(0, m_l, 0),
                        Point3(m_L, m_l, 0),
                        Point3(m_L, 0, 0)
                    }
    );
	for(unsigned int i = 0; i < 4; ++i)
	{
		m_points.push_back(m_points.at(i) + Vec3::Z*m_h);
	}
}

Cube::Cube(const Point3 &o, const double &l, const double &L, const double &h, const Vec3 &normal): m_l(l), m_L(L), m_h(h)
{
	createCube();
	create(o, normal);
}

Cube::Cube(const Point3 &a, const Point3 &b, const Point3 &c, const Point3 &d, const double& l, const Vec3 &normal) : m_l(l)
{
    m_points.insert(m_points.end(), {a, b, c, d});
	for(unsigned int i = 0; i < 4; ++i)
	{
		m_points.push_back(m_points.at(i) + normal*l);
	}
}

std::vector<indexes> Cube::toTriangles()
{
	return pts;
}

std::vector<indexes> Cube::getIndexes()
{
	return pts;
}

Cube::~Cube()
{
	m_points.clear();
}

