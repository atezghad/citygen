#ifndef PRISM_H
#define PRISM_H

#include <Shapes/Shape3.hpp>

/**
 * @class Prism
 * @brief Permet de représenter un prisme
 *
 */
class Prism: public Shape3
{
	public:
		/**
		 * @brief Prism
		 * @param a Point de base du Prisme
		 * @param b	Point de base du Prisme
		 * @param c	Point de base du Prisme
		 * @param h Hauteur
		 */
		Prism(const Point3 &a, const Point3 &b, const Point3 &c, const double &h);

		/**
		 * @brief Prism
		 * @param a Point du Prism
		 * @param b	Point du Prism
		 * @param c	Point du Prism
		 * @param d	Point du Prism
		 * @param e	Point du Prism
		 * @param f	Point du Prism
		 */
		Prism(const Point3 &a, const Point3 &b, const Point3 &c, const Point3 &d, const Point3 &e, const Point3 &f );

		std::vector<indexes> toTriangles() override;

		/**
		 * @brief Retourne l'ordre des points pour générations des triangles
		 * @return Ordre des points pour générations des triangles
		 */
		static std::vector<indexes> getIndexes();

        ~Prism() override;

	private:
		static std::vector<indexes> pts;
		double m_h;
		void create(const Point3 &o, const Vec3 &normal) override;

};

#endif // PRISM_H
