#define _USE_MATH_DEFINES
#include <vector>
#include <utility>
#include <algorithm>
#include <fstream>
#include <cassert>

#include <Shapes/Triangle.hpp>
#include <Shapes/Quad.hpp>
#include <Utils/utils.hpp>
#include <Utils/Random.hpp>
#include <Utils/subdiv.hpp>

char Triangle::_symbol = 'T';

Triangle::Triangle()
{}

Triangle::Triangle(const Point2 &a, const Point2 &b, const Point2 &c)
{
	m_points.push_back(a);
	m_points.push_back(b);
	m_points.push_back(c);
	setCenter();
	setNormals();
}

Triangle::Triangle(const Triangle &t)
{
	m_points = t.m_points;
	m_normals = t.m_normals;
	m_center = t.m_center;
}

Triangle::Triangle(Triangle &&t)
{
	m_points = t.m_points;
	m_normals = t.m_normals;
	m_center = t.m_center;
	t.m_points.clear();
	t.m_normals.clear();
}


Triangle::Triangle(Shape2 *t)
{
	if(t->points().size() == 3)
	{
		m_points = t->points();
		m_normals = t->normals();
		m_center = t->center();
	}
}


Triangle::~Triangle()
{
	m_points.clear();
	m_normals.clear();
}

Triangle& Triangle::operator =(const Triangle &t)
{
	m_points = t.m_points;
	m_normals = t.m_normals;
	m_center = t.m_center;
	return *this;
}

bool Triangle::operator ==(const Triangle &t) const
{
	return (m_points[0] == t.m_points[0]
			&& m_points[1] == t.m_points[1]
			&& m_points[2] == t.m_points[2]
			);
}

bool Triangle::operator !=(const Triangle &t) const
{
	return (!(*this == t ));
}


double Triangle::area() const
{
	return utils::geom::triangleArea(perimeter(), m_points[0], m_points[1], m_points[2]);
}

bool Triangle::validForm(const std::vector<Point2> &v)
{
	return(   (!utils::geom::onTriangle(m_points[0], m_points[1], m_points[2], v[0]))
			&&(!utils::geom::onTriangle(m_points[0], m_points[1], m_points[2], v[1]))
			&&(!utils::geom::onTriangle(m_points[0], m_points[1], m_points[2], v[2])));
}

axShape Triangle::chooseRule()
{
	return Grammar::getInstance().chooseRule<axShape>(_symbol);
}

void Triangle::setCenter()
{
	double p = perimeter();
	double a = m_points[1].distance(m_points[2]);
	double b = m_points[2].distance(m_points[0]);
	double c = m_points[0].distance(m_points[1]);
	m_center = (a/p)* m_points[0] + (b/p)*m_points[1] + (c/p)*m_points[2];
}

std::list<Shape2 *> Triangle::specificSubdivide(const axShape &chosen, const bool &road)
{
	Triangle remnant= *this;
	std::list<Shape2 *> subdivs;
	for(std::vector<char>::const_iterator r = chosen.begin(); r != chosen.end(); ++r)
	{
		if(remnant.canSubdivide(road))
		{
			if(*r == _symbol)
				subdiv::triangleToTriangle(&remnant, subdivs);
			else if(*r == Quad::symbol())
				subdiv::triangleToQuad(&remnant, subdivs);
		}
		else break;
	}
	subdivs.push_back(new Triangle(remnant));
    return subdivs;
}

bool Triangle::inside(const Point2 &p) const
{
    Vec2 AB(m_points[0], m_points[1]);
    Vec2 AP(m_points[0], p);
    Vec2 CA(m_points[2], m_points[0]);
    Vec2 CP(m_points[2], p);
    Vec2 BC(m_points[1], m_points[2]);
    Vec2 BP(m_points[1], p);

    double det1 = (AB.cross(AP)).z();
    double det2 = (CA.cross(CP)).z();
    double det3 = (BC.cross(BP)).z();

    if(det1 == 0. || det2 == 0. || det3 == 0.)
    {
        return (utils::geom::aligned(m_points[0], m_points[1], p)
                || utils::geom::aligned(m_points[0], m_points[2], p)
                || utils::geom::aligned(m_points[1], m_points[2], p));
    }

    return((det1 > 0 && det2 > 0 && det3 > 0) || (det1 < 0 && det2 < 0 && det3 < 0));
}

char Triangle::symbol()
{
	return _symbol;
}

bool Triangle::checkRules() const
{
	return Grammar::getInstance().checkRules(_symbol);
}

double Triangle::shapeIndex() const
{
	double a = area();
    double r = 2.0 * a /perimeter();
    double as = M_PI * r * r;
    return a / as;
}

double Triangle::idealShapeIndex() const
{
	return Parameters::getInstance().get("TRIANGLE_INDEX");
}

Point2 Triangle::randomPoint()
{
	double r1 = RandomGen::getInstance().getDouble(.35, .65);
	double r2 = RandomGen::getInstance().getDouble(.35, .65);
    double sqrt_r1 = std::sqrt(r1);
    return Point2((1.0 - sqrt_r1) * m_points[0] +
            (sqrt_r1 * (1.0 - r2)) * m_points[1] +
            (sqrt_r1 * r2) * m_points[2]);
}
