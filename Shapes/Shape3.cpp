#include <fstream>

#include "Shape3.hpp"

void Shape3::exportVertices(std::ofstream &obj)
{
	for(unsigned int i = 0; i < m_points.size(); ++i)
		obj << "v " << m_points[i].x() << " " <<  m_points[i].y() << " " << m_points[i].z() << std::endl;
}

void Shape3::exportFaces(unsigned int &index, std::ofstream &obj)
{
	drawTriangles(toTriangles(), m_points.size(), index, obj);
}

void Shape3::drawTriangles(const std::vector<indexes> &vec, const unsigned int &nb_points, unsigned int &index, std::ofstream &obj)
{
	unsigned int _index = index + 1;
	for(unsigned int i = 0; i < vec.size(); ++i)
	{
		obj << "f " << _index + vec[i].a << " " << _index + vec[i].c << " " << _index + vec[i].b << std::endl;
	}
	index += nb_points;
}

void Shape3::move(const Vec3 &t)
{
	for(unsigned int i = 0; i < m_points.size(); ++i)
	{
		m_points.at(i) += t;
	}
}

void Shape3::rotate(const Vec3 &axis, const double &angle)
{
	for(unsigned int i = 0; i < m_points.size(); ++i)
	{
		m_points.at(i).rotate(axis, angle);
    }
}

Shape3::~Shape3()
{

}
