#ifndef PRIMITIVE_H
#define PRIMITIVE_H

#include <vector>

#include <Utils/Geometry/Vec3.hpp>
#include <Utils/structs.hpp>

/**
 * @class Shape3
 * @brief Permet de répreseter une forme 3D de manière générique
 */
class Shape3
{
	public:

		/**
		 * @brief Permet d'exporter les points de la forme
		 * @param obj Le fichier
		 */
		void exportVertices(std::ofstream &obj);

		/**
		 * @brief  Permet d'exporter les faces de la forme
		 * @param[in][out] index La valeur de l'indice actuel
		 * @param[in][out] obj Le fichier
		 */
		void exportFaces(unsigned int &index, std::ofstream &obj) ;

		/**
		 * @brief Permet de déplacer la forme
		 * @param Le vecteur
		 */
		void move(const Vec3 &t);

		/**
		 * @brief Permet d'effectuer une rotation
		 * @param axis L'axe de rotation
		 * @param angle L'angle de rotation
		 */
		void rotate(const Vec3 &axis, const double &angle);

		/**
		 * @brief Retourne l'ordre des points pour générations des triangles
		 * @return Ordre des points pour générations des triangles
		 */
		virtual std::vector<indexes> toTriangles() = 0;

        virtual ~Shape3();

	protected:
		std::vector<Point3> m_points;

		void drawTriangles(const std::vector<indexes> &vec, const unsigned int &nb_points, unsigned int &index, std::ofstream &obj);

		/**
		 * @brief Permet de créer une forme 3D
		 * @param o Point d'origine
		 * @param normal Vecteur normal au plan
		 */
		virtual void create(const Point3 &o, const Vec3 &normal) = 0;

};

#endif // PRIMITIVE_H
