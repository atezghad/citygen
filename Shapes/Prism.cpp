#include <algorithm>

#include "Prism.hpp"

std::vector<indexes> Prism::pts   =   {{0,1,2},{3,4,5},
									  {0,1,4},{0,4,3},
									  {1,2,5},{1,5,4},
									  {2,0,3},{2,3,5}};

void Prism::create(const Point3 &o, const Vec3 &normal)
{
	(void)o;
	for(unsigned int i = 0; i < 3; ++i)
	{
		m_points.push_back(m_points[i] + normal*m_h);
	}
}

Prism::Prism(const Point3 &a, const Point3 &b, const Point3 &c, const double &h): m_h(h)
{
	Vec3 ab(a,b);
	Vec3 ac(a,c);
	Vec3 n = ab.cross(ac).normalize();
	m_points.push_back(a);
	if(n.z() < 0)
	{
		m_points.push_back(c);
		m_points.push_back(b);
	}
	else if(n.z() > 0)
	{
		m_points.push_back(b);
		m_points.push_back(c);
	}
	create(a, n);
}

Prism::Prism(const Point3 &a, const Point3 &b, const Point3 &c, const Point3 &d, const Point3 &e, const Point3 &f )
{
	Vec3 ab(a,b);
	Vec3 ac(a,c);
	Vec3 n = ab.cross(ac).normalize();
	m_points.push_back(a);
	if(n.z() < 0)
	{
        m_points.insert(m_points.end(), {c, b, d, f, e});
	}
	else if(n.z() > 0)
	{
        m_points.insert(m_points.end(), {b, c, d, e, f});
	}
}

std::vector<indexes> Prism::toTriangles()
{
	return pts;
}

std::vector<indexes> Prism::getIndexes()
{
	return pts;
}

Prism::~Prism()
{
	m_points.clear();
}

