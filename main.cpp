#include <iostream>
#include <chrono>

#include <Mesh/Mesh.hpp>

void usage(char * argv)
{
	std::cout << argv << " origin.X origin.Y end.X end.Y : [Points de départ et d'arrivée]" << std::endl
			  << "rules_filename parameters_filename elements_filename [Fichiers de grammaire | paramètres | parametres architecturaux" << std::endl
			  << "building_obj_filename [pavements_obj_filename] : [Fichier .OBJ de batiment (et optionellement les trottoirs)]" << std::endl;
}

int main(int argc, char *argv[])
{
	if(argc != 9 && argc != 10)
	{
		usage(argv[0]);
		return EXIT_SUCCESS;
	}
	else
	{
		Point2 o(std::stod(argv[1]), std::stod(argv[2]));
		Point2 a(std::stod(argv[3]), std::stod(argv[4]));
		Mesh m(o, a, argv[5], argv[6], argv[7]);
		auto start = std::chrono::high_resolution_clock::now();
		m.generateCity();
		auto finish = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> elapsed = finish - start;
		std::cout << "Durée de la génération: " << elapsed.count() << " s" << std::endl;
		start = std::chrono::high_resolution_clock::now();
		if(argc == 10)
			m.blocksToOBJ(argv[9]);
		m.buildingsToOBJ(argv[8]);
		finish = std::chrono::high_resolution_clock::now();
		elapsed = finish - start;
		std::cout << "Durée de l'export: " << elapsed.count() << " s" << std::endl;
		return EXIT_SUCCESS;
	}
}
