#include <Building/Ground.hpp>
#include <Building/Floor.hpp>
#include <Shapes/Quad.hpp>
#include <Building/Roof.hpp>
#include <Shapes/Triangle.hpp>
#include <Utils/utils.hpp>
#include <Utils/Random.hpp>

char Ground::_symbol = 'G';

Ground::Ground(Shape2 *base, Building *build) : m_building(build)
{
	initBase(base);
	m_height_bottom = 0;
}

Ground::~Ground()
{
	delete _base;
}

Ground::Ground(const Ground &t)
{
	initBase(t._base);
	m_height_bottom = t.m_height_bottom;
	m_height_top = t.m_height_top;
}

Ground::Ground(Ground &&t)
{
	initBase(t._base);
	delete (t._base);
	m_height_bottom = t.m_height_bottom;
	m_height_top = t.m_height_top;
}

Ground& Ground::operator =(Ground &t)
{
	initBase(t._base);
	m_height_bottom = t.m_height_bottom;
	m_height_top = t.m_height_top;
	return *this;
}

axLevel Ground::chooseRule()
{
	return Grammar::getInstance().chooseRule<axLevel>(_symbol);
}

bool Ground::canGenerate() const
{
	return !(_symbol == Roof::symbol());
}

Level* Ground::specificGenerate(const axLevel &rule)
{
	float l = std::get<1>(rule);
	float h = std::get<2>(rule);
	m_height_top = (h == 0)? RandomGen::getInstance().getDouble(1.90, 4.0) : h;
	Shape2 * nbase = _base;
	Level * result = nullptr;
	if(l != 0)
	{
		switch (_base->points().size())
		{
			case 3:
				nbase = new Triangle(_base);
				break;
			case 4:
				nbase = new Quad(_base);
				break;
		}
		nbase->shrink(-l);
	}
	switch (_base->points().size())
	{
		case 3:
			result =  new Floor(new Triangle(nbase), m_height_top + Parameters::getInstance().get("GAP"), m_building);
			break;
		case 4:
			result = new Floor(new Quad(nbase), m_height_top + Parameters::getInstance().get("GAP"), m_building);
			break;
	}
	if(l != 0)
		delete nbase;
	return result;
}

char Ground::symbol()
{
	return _symbol;
}

void Ground::generateArchElements(std::list<Shape3*> &l)
{
    Point3 a,b,c;
    Plane p;
    std::vector<Point2> m_points = _base->points();
    unsigned int nbpoints = m_points.size();
    std::pair<std::list<int>, double> elements = m_building->whichElements(symbol());
    double dNbElements, remainder;
    int iNbElements;
    for(unsigned int i = 0; i < _base->points().size(); ++i)
    {
        a = Point3(m_points[i].x(), m_height_bottom,m_points[i].y() );
        b = Point3(m_points[(i+1) % nbpoints].x(), m_height_bottom, m_points[(i+1) % nbpoints].y());
        c = Point3(a.x(), m_height_top, a.z());
        p = makePlane(a, b, c);
        dNbElements = a.distance(b)/elements.second;
        iNbElements = dNbElements;
        remainder = dNbElements - iNbElements;
        remainder = (remainder != 0.0)? 1 : 0;
        if(p.normalInside)
        {
            for(int j = 0; j < iNbElements + remainder; ++j)
            {
                for(auto it = elements.first.begin(); it != elements.first.end(); ++it)
                {
                    ArchiElement *e = m_building->getElement(*it);
                    e->placeElement(a, b, c, m_height_top, p, l);
                }
            }
        }
        else
        {
            for(int j = 0; j < iNbElements + remainder; ++j)
            {
                for(auto it = elements.first.begin(); it != elements.first.end(); ++it)
                {
                    ArchiElement *e = m_building->getElement(*it);
                    e->placeElement(b, a, c, m_height_top, p, l);
                }
            }
        }
    }
}
