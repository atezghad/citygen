#include "Roof.hpp"
#include <Utils/Random.hpp>
#include <Utils/utils.hpp>

char Roof::_symbol = 'R';

Roof::Roof(Shape2 *base, const double&h, Building *build) : m_building(build)
{
	initBase(base);
	m_height_bottom = h;
	m_height_top = m_height_bottom + RandomGen::getInstance().getDouble(0, 1);
}

Roof::~Roof()
{
	delete _base;
}

char Roof::symbol()
{
	return _symbol;
}

Roof::Roof(const Roof &t)
{
	initBase(t._base);
	m_height_bottom = t.m_height_bottom;
	m_height_top = t.m_height_top;
}

Roof::Roof(Roof &&t)
{
	initBase(t._base);
	delete t._base;
	m_height_bottom = t.m_height_bottom;
	m_height_top = t.m_height_top;
}

Roof& Roof::operator =(Roof &t)
{
	initBase(t._base);
	m_height_bottom = t.m_height_bottom;
	m_height_top = t.m_height_top;
	return *this;
}

axLevel Roof::chooseRule()
{
	return Grammar::getInstance().chooseRule<axLevel>(_symbol);
}

bool Roof::canGenerate() const
{
	return false;
}

Level* Roof::specificGenerate(const axLevel &rule)
{
	(void)rule;
	return nullptr;
}

void Roof::generateArchElements(std::list<Shape3*> &l)
{
    double r = RandomGen::getInstance().getDouble(0, 1);
    if( r < .5 || _base->points().size() == 3)
    {
        l.splice(l.end(), archi::roofBorders(_base, m_height_top));
    }
    else
    {
        double max = -1, current;
        unsigned int start = 0;
        std::vector<Point2> points = _base->points();
        for(unsigned int i = 0; i < 4; ++i)
        {
            current = points.at(i).distance(points.at((i+1)%4));
            if(current > max)
            {
                max = current;
                start = i;
            }
        }
        unsigned int oppStart        = ( start + 3 ) % 4;
        unsigned int acrossStart     = ( start + 1 ) % 4;
        unsigned int oppAcrossStart  = ( acrossStart + 1 ) % 4;
        Point2 centerStart       = utils::geom::center(points[start], points[oppStart]);
        Point2 centerAcrossStart = utils::geom::center(points[acrossStart], points[oppAcrossStart]);
        Point3 p0(points[start].x(), m_height_top, points[start].y());
        Point3 p1(points[oppStart].x(), m_height_top, points[oppStart].y());
        Point3 p2(centerStart.x(), m_height_top + 1.25, centerStart.y());
        Point3 p3(points[acrossStart].x(), m_height_top, points[acrossStart].y());
        Point3 p4(points[oppAcrossStart].x(), m_height_top, points[oppAcrossStart].y());
        Point3 p5(centerAcrossStart.x(), m_height_top + 1.25, centerAcrossStart.y());
        l.push_back(new Prism(p0, p1, p2, p3, p4, p5));
    }
}
