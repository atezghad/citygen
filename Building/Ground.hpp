#ifndef GROUND_H
#define GROUND_H

#include <Building/Level.hpp>
#include <Building/Building.hpp>

/**
 * @class Ground
 * @brief Permet de représenter un Rez-de-Chaussée
 */
class Ground : public Level
{
	public:
		Ground(Shape2 *base, Building *build);
		~Ground();

		Ground(const Ground &t);
		Ground(Ground &&t);

		Ground& operator =(Ground &t);

		bool canGenerate() const override;
        void generateArchElements(std::list<Shape3*> &l) override;
		axLevel chooseRule() override;
		Level* specificGenerate(const axLevel &rule) override;

		static char symbol();

	private:
		static char _symbol;
		Building *m_building;
};

#endif // GROUND_H
