#ifndef BUILDING_H
#define BUILDING_H

#include <list>
#include <map>

#include <Building/Level.hpp>
#include <Architecture/ArchiElement.hpp>

/**
 * @class Building
 * @brief Permet de représenter un bâtiment
 */
class Building
{
	public:
		/**
		 * @brief Building
		 * @param ground La surface de base
		 */
		Building(Shape2 *ground);
		~Building();

        /**
         * @brief Permet de générer les élémennts architecturax des différents niveaux
         * @param l Liste de volumes
         */
        void generateArchElements(std::list<Shape3*> &l);

		/**
		 * @brief Permet d'exporter les points de la forme
		 * @param obj Le fichier
		 */
		void exportVertices(std::ofstream &obj);

		/**
		 * @brief  Permet d'exporter les faces de la forme
		 * @param[in][out] index La valeur de l'indice actuel
		 * @param[in][out] obj Le fichier
		 */
		void exportFaces(unsigned int &index, std::ofstream &obj);

		/**
		 * @brief Permet de récuperer les éléments architecturaux selon le type de niveau
		 * @param symbol Le symbole représentant le niveau
		 * @return la liste des éléments ainsi que la largeur totale
		 */
		std::pair<std::list<int>, double> whichElements(const char symbol) const;

		/**
		 * @brief Permet de récuperer les éléments architecturaux selon le type de niveau
		 * @param e Le type d'élément
		 * @return Un pointeur vers l'élément architectural
		 */
		ArchiElement* getElement(const int &e) const;

	private:
		std::list<Level*> m_levels;
		std::map<char, std::pair<std::list<int>, double >> m_archi;
		std::map<int, ArchiElement*> m_elemParam;
		void generateLevels();
		void chooseArchiElements();
		void chooseArchiElementsOrder();
		void addArchiElement(const int i);
		void transformRule(const char symbol, const axArchi &rule);
};

#endif // BUILDING_H
