#include <Building/Building.hpp>
#include <Building/Ground.hpp>
#include <Building/Floor.hpp>
#include <Config/Grammar.hpp>
#include <Architecture/archi.hpp>
#include <Config/Elements.hpp>
#include <Architecture/Window.hpp>

Building::Building(Shape2 *ground)
{
	m_levels.push_back(new Ground(ground, this));
	generateLevels();
	chooseArchiElements();
	chooseArchiElementsOrder();
}

void Building::generateLevels()
{
	m_levels.front()->generate(m_levels);
}

void Building::chooseArchiElements()
{
	addArchiElement(archi::DOOR);
	addArchiElement(archi::BALCON);
	addArchiElement(archi::WINDOW);
	addArchiElement(archi::COLUMN);
}

void Building::chooseArchiElementsOrder()
{
	axArchi archiGround = Grammar::getInstance().chooseRule<axArchi>(Ground::symbol(), false);
	axArchi archiFloor = Grammar::getInstance().chooseRule<axArchi>(Floor::symbol(), false);
	transformRule(Ground::symbol(), archiGround);
	transformRule(Floor::symbol(), archiFloor);
}

void Building::addArchiElement(const int i)
{

	m_elemParam.insert(std::make_pair(i, Elements::get(i)));
}

ArchiElement* Building::getElement(const int &e) const
{
	return m_elemParam.at(e);
}

void Building::transformRule(const char symbol, const axArchi &rule)
{
	std::list<int> to_insert;
	double totalWidth = 0;
	for(auto it = rule.begin(); it != rule.end(); ++it)
	{
		int e;
		if(*it == 'W')
			e = archi::WINDOW;
		else if(*it == 'D')
			e = archi::DOOR;
		else if(*it == 'B')
			e = archi::BALCON;
		else if(*it == 'C')
			e = archi::COLUMN;
		totalWidth += m_elemParam.at(e)->width();
		to_insert.push_back(e);
	}
	m_archi.insert(std::make_pair(symbol, std::make_pair(to_insert, totalWidth)));
}

std::pair<std::list<int>,double> Building::whichElements(const char symbol) const
{
	return m_archi.at(symbol);
}

void Building::generateArchElements(std::list<Shape3*> &l)
{
    for(std::list<Level*>::iterator it = m_levels.begin(); it != m_levels.end(); ++it)
            (*it)->generateArchElements(l);
}

void Building::exportVertices(std::ofstream &obj)
{
	for(std::list<Level*>::iterator it = m_levels.begin(); it != m_levels.end(); ++it)
		(*it)->exportVertices(obj);
}

void Building::exportFaces(unsigned int &index, std::ofstream &obj)
{
	for(std::list<Level*>::iterator it = m_levels.begin(); it != m_levels.end(); ++it)
		(*it)->exportFaces(index, obj);
}

Building::~Building()
{
	for(std::list<Level*>::iterator it = m_levels.begin(); it != m_levels.end(); ++it)
		delete *it;
}
