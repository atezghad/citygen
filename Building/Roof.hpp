#ifndef ROOF_H
#define ROOF_H

#include <Building/Level.hpp>
#include <Building/Building.hpp>

/**
 * @class Roof
 * @brief Permet de représenter un Toit
 */
class Roof : public Level
{
	public:
		Roof(Shape2 *base, const double&h, Building *build);
		~Roof();

		Roof(const Roof &t);
		Roof(Roof &&t);

		Roof& operator =(Roof &t);
		bool operator==(Roof &t);
		bool operator!=(Roof &t);

		bool canGenerate() const override;
        void generateArchElements(std::list<Shape3*> &l) override;
		axLevel chooseRule() override;
		Level* specificGenerate(const axLevel &rule) override;

		static char symbol();

	private:
		static char _symbol;
		Building * m_building;
};

#endif // ROOF_H
