#include <Building/Floor.hpp>
#include <Building/Roof.hpp>
#include <Utils/Random.hpp>
#include <Utils/utils.hpp>
#include <Utils/Geometry/Plane.hpp>
#include <Architecture/archi.hpp>
#include <Shapes/Triangle.hpp>
#include <Shapes/Quad.hpp>

char Floor::_symbol = 'F';

Floor::Floor(Shape2 *base, const double&h, Building *build) : m_building(build)
{
	m_height_bottom = h;
	initBase(base);
}

Floor::~Floor()
{
	delete _base;
}

Floor::Floor(const Floor &t)
{
	m_height_bottom = t.m_height_bottom;
	m_height_top = t.m_height_top;
	initBase(t._base);

}

Floor::Floor(Floor &&t)
{
	m_height_bottom = t.m_height_bottom;
	m_height_top = t.m_height_top;
	initBase(t._base);
	delete t._base;
}

Floor& Floor::operator =(Floor &t)
{
	m_height_bottom = t.m_height_bottom;
	m_height_top = t.m_height_top;
	initBase(t._base);
	return *this;
}

axLevel Floor::chooseRule()
{
	return Grammar::getInstance().chooseRule<axLevel>(_symbol);
}

bool Floor::canGenerate() const
{
	return !(_symbol == Roof::symbol());
}

Level* Floor::specificGenerate(const axLevel &rule)
{
	float l = std::get<1>(rule);
	float h = std::get<2>(rule);
	m_height_top = m_height_bottom + h;
	Shape2 * nbase = _base;
	Level * result = nullptr;
	if(l != 0)
	{
		switch (_base->points().size())
		{
			case 3:
				nbase = new Triangle(_base);
				break;
			case 4:
				nbase = new Quad(_base);
				break;
		}
		nbase->shrink(-l);
	}
	if(std::get<0>(rule) == Roof::symbol())
	{
		switch (_base->points().size())
		{
			case 3:
				result =  new Roof(new Triangle(nbase), m_height_top + Parameters::getInstance().get("GAP"), m_building);
				break;
			case 4:
				result =  new Roof(new Quad(nbase), m_height_top + Parameters::getInstance().get("GAP"), m_building);
				break;
		}
	}
	else
	{
		switch (_base->points().size())
		{
			case 3:
				result =  new Floor(new Triangle(nbase), m_height_top + Parameters::getInstance().get("GAP"), m_building);
				break;
			case 4:
				result =  new Floor(new Quad(nbase), m_height_top + Parameters::getInstance().get("GAP"), m_building);
				break;
		}
	}
	if(l != 0)
		delete nbase;
	return result;
}

char Floor::symbol()
{
	return _symbol;
}

void Floor::generateArchElements(std::list<Shape3*> &l)
{
    Point3 a,b,c;
    Plane p;
    std::vector<Point2> m_points = _base->points();
    unsigned int nbpoints = m_points.size();
    std::pair<std::list<int>, double> elements = m_building->whichElements(symbol());
    double dNbElements, remainder;
    int iNbElements;
    for(unsigned int i = 0; i < _base->points().size(); ++i)
    {
        a = Point3(m_points[i].x(), m_height_bottom,m_points[i].y() );
        b = Point3(m_points[(i+1) % nbpoints].x(), m_height_bottom, m_points[(i+1) % nbpoints].y());
        c = Point3(a.x(), m_height_top, a.z());
        p = makePlane(a, b, c);
        dNbElements = a.distance(b)/elements.second;
        iNbElements = dNbElements;
        remainder = dNbElements - iNbElements;
        remainder = (remainder != 0.0)? 1 : 0;
        if(p.normalInside)
        {
            for(unsigned int j = 0; j < iNbElements + remainder; ++j)
            {
                for(auto it = elements.first.begin(); it != elements.first.end(); ++it)
                {
                    ArchiElement *e = m_building->getElement(*it);
                    e->placeElement(a, b, c, m_height_top, p, l);
                }
            }
        }
        else
        {
            for(unsigned int j = 0; j < iNbElements + remainder; ++j)
            {
                for(auto it = elements.first.begin(); it != elements.first.end(); ++it)
                {
                    ArchiElement *e = m_building->getElement(*it);
                    e->placeElement(b, a, c, m_height_top, p, l);
                }
            }
        }
    }
}
