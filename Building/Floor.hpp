#ifndef FLOOR_H
#define FLOOR_H

#include <Building/Level.hpp>
#include <Building/Building.hpp>

/**
 * @class Floor
 * @brief Permet de représenter un Etage
 */
class Floor : public Level
{
	public:
		Floor(Shape2 *base, const double&h, Building *build);
		~Floor();

		Floor(const Floor &t);
		Floor(Floor &&t);

		Floor& operator =(Floor &t);

		bool canGenerate() const override;
        void generateArchElements(std::list<Shape3*> &l) override;
		axLevel chooseRule() override;
		Level* specificGenerate(const axLevel &rule) override;

		static char symbol();

	private:
		static char _symbol;
		Building * m_building;
};

#endif // FLOOR_H
