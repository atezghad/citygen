#ifndef LEVEL_H
#define LEVEL_H

#include <fstream>
#include <list>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include <Config/Grammar.hpp>
#include <Config/Parameters.hpp>
#include <Utils/structs.hpp>
#include <Utils/Geometry/Plane.hpp>
#include <Shapes/Shape2.hpp>
#include <Shapes/Cube.hpp>
#include <Shapes/Prism.hpp>

/**
 * @class Level
 * @brief Permet de représenter le niveau d'un Building
 */
class Level
{
	public:
		/**
		 * @brief Permet de choisir une des règles définies
		 * @return La règle choisie pour générer le prochain niveau
		 */
		virtual axLevel chooseRule() = 0;

		/**
		 * @brief Vérifie si un niveau peut en engendrer un autre
		 * @return True si génération possible, false sinon
		 */
		virtual bool canGenerate() const = 0;

		/**
		 * @brief Réalise une génération spécifique au niveau
		 * @param rule La règle choisie pour la génération
		 * @return Le niveau généré
		 */
		virtual Level* specificGenerate(const axLevel &rule) = 0;
		virtual ~Level(){}

		/**
		 * @brief operator ==
		 * @param t
		 * @return
		 */
		bool operator==(const Level *t);

		/**
		 * @brief operator !=
		 * @param t
		 * @return
		 */
		bool operator!=(const Level *t);

		/**
		 * @brief Permet d'exporter les points de la forme
		 * @param obj Le fichier
		 */
		virtual void exportVertices(std::ofstream &obj);

		/**
		 * @brief  Permet d'exporter les faces de la forme
		 * @param[in][out] index La valeur de l'indice actuel
		 * @param[in][out] obj Le fichier
		 */
		virtual void exportFaces(unsigned int &index, std::ofstream &obj);

		/**
		 * @brief Permet de générer un niveau
		 * @param m Le conteneur de niveaux
		 */
		void generate(std::list<Level*> &m);

        /**
         * @brief Permet de génerer et placer les éléments architecturaux
         * @param l Le conteneur de volumes
         */
        virtual void generateArchElements(std::list<Shape3*> &l) = 0;


	protected:
		Shape2 * _base;
		static std::vector<indexes> order;
		double m_height_bottom;
		double m_height_top;
		void initBase(Shape2 *base);
};

void drawTriangles(const std::vector<indexes> &vec, const unsigned int &nb_points, unsigned int &index, std::ofstream &obj);
#endif // LEVEL_H
