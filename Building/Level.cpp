#include <Building/Level.hpp>
#include <Shapes/Triangle.hpp>
#include <Shapes/Quad.hpp>
#include <Utils/Geometry/Vec2.hpp>
#include <Utils/utils.hpp>
#include <Architecture/archi.hpp>
#include <Shapes/Cube.hpp>
#include <Shapes/Prism.hpp>

void Level::generate(std::list<Level*> &m)
{
    if(canGenerate() && m_height_top < Parameters::get("MAX_HEIGHT"))
	{
		Level* _new = specificGenerate(chooseRule());
        m.push_back(_new);
        _new->generate(m);
	}
}

void Level::initBase(Shape2 *base)
{
	unsigned int nbp = base->points().size();
	switch(nbp)
	{
		case 3:
			_base = new Triangle(base);
			break;
		case 4:
			_base = new Quad(base);
			break;
	}
}

bool Level::operator ==(const Level * t)
{
	std::vector<Point2> m_points = _base->points();
	std::vector<Point2> t_points = t->_base->points();
	if(m_points.size() == t_points.size())
	{
		for(unsigned int i = 0; i < m_points.size(); ++i)
		{
			if(m_points[i] != t_points[i])
				return false;
		}
		return true;
	}
	return false;
}

bool Level::operator !=(const Level * t)
{
	return !((*this) == t);
}

void Level::exportVertices(std::ofstream &obj)
{
	std::vector<Point2> m_points = _base->points();
	for(unsigned int i = 0; i < m_points.size(); ++i)
		obj << "v " << m_points[i].x() << " " <<  m_height_bottom << " " << m_points[i].y() << std::endl;
	for(unsigned int i = 0; i < m_points.size(); ++i)
		obj << "v " << m_points[i].x() << " " <<  m_height_top << " " << m_points[i].y() << std::endl;
}


void Level::exportFaces(unsigned int &index, std::ofstream &obj)
{
	std::vector<indexes> order;
    unsigned int nb_points = 0;
	switch(_base->points().size())
	{
		case 3:
			order = Prism::getIndexes();
            nb_points = 6;
			break;
		case 4:
			order = Cube::getIndexes();
            nb_points = 8;
			break;
	}
    drawTriangles(order, nb_points, index, obj);
}


void drawTriangles(const std::vector<indexes> &vec, const unsigned int &nb_points, unsigned int &index, std::ofstream &obj)
{
	unsigned int _index = index + 1;
	for(unsigned int i = 0; i < vec.size(); ++i)
	{
		obj << "f " << _index + vec[i].a << " " << _index + vec[i].c << " " << _index + vec[i].b << std::endl;
	}
	index += nb_points;
}
