#include <algorithm>

#include <Utils/subdiv.hpp>
#include <Utils/Random.hpp>
#include <Shapes/Quad.hpp>
#include <Shapes/Triangle.hpp>

void subdiv::quadToQuad(Shape2 *remnant_q, std::list<Shape2 *> &subdivs)
{
	Quad *remnant = (Quad*)remnant_q;
	int side = RandomGen::getInstance().getInt(0, 3);
	int other_side = (side < 2)? side + 2 : side - 2;
    int o_side = (side + 1) % 4;
    if(remnant->at(side).distance(remnant->at(o_side)) < remnant->at(o_side).distance(remnant->at((side+2)%4)))
	{
        side  = o_side;
		other_side = (side < 2)? side + 2 : side - 2;
	}

	Point2 r1 = randomMid(remnant->at(side), remnant->at((side+1)%4));
	Point2 r2 = randomMid(remnant->at(other_side), remnant->at((other_side+1)%4));

	switch(side)
	{
		case 0:
			subdivs.push_back(new Quad(remnant->at(0), r1, r2, remnant->at(3)));
			remnant->set(r1, 0);
			remnant->set(r2, 3);
			break;
		case 1:
			subdivs.push_back(new Quad(remnant->at(0), remnant->at(1), r1, r2));
			remnant->set(r2, 0);
			remnant->set(r1, 1);
			break;
		case 2:
			subdivs.push_back(new Quad(remnant->at(0), r2, r1, remnant->at(3)));
			remnant->set(r2, 0);
			remnant->set(r1, 3);
			break;
		case 3:
			subdivs.push_back(new Quad(remnant->at(0), remnant->at(1), r2, r1));
			remnant->set(r1, 0);
			remnant->set(r2, 1);
			break;
	}
}

Shape2* subdiv::quadToTriangle(Shape2 *remnant_q, std::list<Shape2 *> &subdivs)
{
	Quad *remnant = (Quad*)remnant_q;
	int pt0 = RandomGen::getInstance().getInt(0, 3);
	int pt1 = (pt0 + 2) % 4;

	int pt2 = (pt0 + 1) % 4;
	int pt3 = (pt2 + 2) % 4;

	subdivs.push_back(new Triangle(remnant->at(pt0), remnant->at(pt1), remnant->at(pt3)));
	return new Triangle(remnant->at(pt0), remnant->at(pt2), remnant->at(pt1));
}

void subdiv::triangleToTriangle(Shape2* remnant_t, std::list<Shape2 *> &subdivs)
{
	Triangle *remnant = (Triangle*)remnant_t;
	int pt0 = RandomGen::getInstance().getInt(0, 2);
	int pt1 = (pt0 + 1) % 3;
	int pt2 = (pt0 + 2) % 3;
	if(remnant->at(pt1).distance(remnant->at(pt2)) < remnant->at(pt0).distance(remnant->at(pt1)))
	{
		pt0 = (pt0 + 1) % 3;
		pt1 = (pt0 + 1) % 3;
		pt2 = (pt0 + 2) % 3;
	}

	Point2 r1 = randomMid(remnant->at(pt1), remnant->at(pt2));
	subdivs.push_back(new Triangle(remnant->at(pt0),  r1, remnant->at(pt2)));

	Point2 old0 = remnant->at(pt0);
	Point2 old1 = remnant->at(pt1);

	remnant->set(old0, 0);
	remnant->set(old1, 1);
	remnant->set(r1, 2);
}

void subdiv::triangleToQuad(Shape2* remnant_t, std::list<Shape2 *> &subdivs)
{
	Triangle *remnant = (Triangle*)remnant_t;
	int side = RandomGen::getInstance().getInt(0, 2);
    int o_side = ( side + 1) % 3;
    if(remnant->at(side).distance(remnant->at(o_side)) < remnant->at(o_side).distance(remnant->at((side+2)%3)))
    {
        side = o_side;
    }
	int direction = RandomGen::getInstance().getInt(1, 2);
	int other_side = (side + direction) % 3;

    Point2 r1 = randomMid(remnant->at(side), remnant->at((side + 1) % 3));
    Point2 r2 = randomMid(remnant->at(other_side), remnant->at((other_side + 1) % 3));

    Point2 old = remnant->at((direction == 1)? other_side : side);
	switch(direction)
	{
		case 1:
		{
			switch(side)
			{
				case 0:
					subdivs.push_back(new Quad(remnant->at(0), r1, r2, remnant->at(2)));
					break;
				case 1:
					subdivs.push_back(new Quad(remnant->at(0), remnant->at(1), r1, r2));
					break;
				case 2:
					subdivs.push_back(new Quad(remnant->at(1), remnant->at(2), r1, r2));
					break;
			}
			remnant->set(r1, 0);
			remnant->set(old, 1);
			remnant->set(r2, 2);
			break;
		}
		case 2:
		{

			switch(side)
			{
				case 0:
					subdivs.push_back(new Quad(remnant->at(1), remnant->at(2), r2, r1));
					break;
				case 1:
					subdivs.push_back(new Quad(remnant->at(2), remnant->at(0), r2, r1));
					break;
				case 2:
					subdivs.push_back(new Quad(remnant->at(0), remnant->at(1), r2, r1));
					break;
			}
			remnant->set(r2, 0);
			remnant->set(old, 1);
			remnant->set(r1, 2);
			break;
		}
	}
}
