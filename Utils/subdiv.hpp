/**
* \file subdiv.hpp
* \author Anis Tezghadanti
* */
#ifndef SUBDIV_HPP
#define SUBDIV_HPP

#include <Shapes/Shape2.hpp>

/**
 * \namespace subdiv
 * \brief Contient les fonctions permettant de subdiviser une Forme
 * */
namespace subdiv
{
	/**
	 * @brief Permet de générer un Quad à partir d'un Quad
	 * @param remnant_q Le Quad à subdiviser
	 * @param subdivs Le conteneur de Formes
	 */
	void     quadToQuad         (Shape2 *remnant_q, std::list<Shape2 *> &subdivs);

	/**
	 * @brief Permet de générer un Triangle à partir d'un Quad
	 * @param remnant_q Le Quad à subdiviser
	 * @param subdivs Le conteneur de Formes
	 * @return Le Triangle restant de la subdivision
	 */
	Shape2*  quadToTriangle     (Shape2 *remnant_q, std::list<Shape2 *> &subdivs);

	/**
	 * @brief Permet de générer un Triangle à partir d'un Triangle
	 * @param remnant_t Le Triangle à subdiviser
	 * @param subdivs Le conteneur de Formes
	 */
	void     triangleToTriangle (Shape2* remnant_t, std::list<Shape2 *> &subdivs);

	/**
	 * @brief Permet de générer un Quad à partir d'un Triangle
	 * @param remnant_q Le Triangle à subdiviser
	 * @param subdivs Le conteneur de Formes
	 */
	void     triangleToQuad     (Shape2* remnant_t, std::list<Shape2 *> &subdivs);
}

#endif // SUBDIV_HPP
