/**
* \file utils.hpp
* \author Anis Tezghadanti
* */
#ifndef UTILS_HPP
#define UTILS_HPP

#include <vector>
#include <string>

#include <Utils/Geometry/Point2.hpp>
#include <Utils/Geometry/Point3.hpp>


/**
 * \namespace utils
 * \brief Contient plusieurs fonctions utilitaires
 * */
namespace utils
{
	/**
	 * @brief Permet de séparer une std::string selon un jeton
	 * @param s La chaîne à séparer
	 * @param delim Le jeton
	 * @return Les chaînes de caractères
	 */
	std::vector<std::string> split(const std::string &s, char delim);

	/**
	 * @brief Permet de récupérer un ligne d'un fichier dont les commentaires commencent par '#'
	 * @param fichier Le fichier à traiter
	 * @return Ligne non commentée
	 */
	std::string noComment(std::ifstream &fichier);

	/**
	 * @brief Convertit des degrés en radians
	 * @param deg L'angle en degrés
	 * @return L'angle en radians
	 */
	double toRadians(const double &deg);

	/**
	 * @brief Convertit des radians en degrés
	 * @param rad L'angle en radians
	 * @return L'angle en degrés
	 */
	double toDegrees(const double &rad);

	/**
	 * \namespace form
	 * \brief Contient les fonctions utilitaires liées à une Forme
	 * */
	namespace form
	{
		/**
		 * @brief Permet de trier les symboles (d'abord Q, puis T)
		 * @param q Symbole du Quad
		 * @param t Symbole du Triangle
		 * @return true si q == 'Q' et t == 'T'
		 */
		bool compare_form(const char q, const char t);
	}

	/**
	 * \namespace geom
	 * \brief Contient les fonctions de calcul géométrique
	 * */
	namespace geom
	{

		/**
        * @brief Teste si un triangle est correctement orienté
        * @param a Point du triangle
        * @param b Point du triangle
        * @param c Point du triangle
        * @return true si correctement orienté, false sinon
        */bool validTriangle(const Point2 &a, const Point2 &b, const Point2 &c);

		/**
		 * @brief Teste si deux segments s'intersectent (Point commun non pris en compte)
		 * @param a Extremité du premier segment
		 * @param b	Extremité du premier segment
		 * @param x	Extremité du deuxième segment
		 * @param y	Extremité du deuxième segment
		 * @return true si intersection, false sinon
		 */
		bool intersectSegments2D(const Point2& a,const Point2 &b, const Point2 &x, const Point2 &y);

		/**
		 * @brief Teste si un Point est sur un segment
		 * @param a Extremité du segment
		 * @param b	Extremité du segment
		 * @param p Un Point
		 * @return true si p est sur [ab], false sinon
		 */
		bool aligned(const Point2 &a, const Point2 &b, const Point2 &p);

		/**
		 * @brief Renvoie le centre d'un segment 2D
		 * @param a Extremité du segment
		 * @param b	Extremité du segment
		 * @return Le centre du segment [ab]
		 */
		Point2 center(const Point2 &a, const Point2 &b);

		/**
		 * @brief Renvoie le centre d'un segment 3D
		 * @param a Extremité du segment
		 * @param b	Extremité du segment
		 * @return Le centre du segment [ab]
		 */
		Point3 center(const Point3 &a, const Point3 &b);

		/**
		 * @brief Teste si un Point se trouve sur un triangle
		 * @param a Point du triangle
		 * @param b	Point du triangle
		 * @param c	Point du triangle
		 * @param p Point
		 * @return true si p se trouve sur ABC, false sinon
		 */
		bool onTriangle(const Point2 &a, const Point2 &b, const Point2 &c, const Point2 &p);

		/**
		 * @brief Calcule l'aire d'un triangle selon la formule de Héron
		 * @param perimeter Perimètre du triangle
		 * @param p0 Point du triangle
		 * @param p1 Point du triangle
		 * @param p2 Point du triangle
		 * @return L'aire du triangle ABC
		 */
		double triangleArea(const double &perimeter, const Point2 &p0, const Point2 &p1, const Point2 &p2);
	}
}


#endif // UTILS_HPP
