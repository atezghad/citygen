#ifndef RANDOMLIB_HPP
#define RANDOMLIB_HPP

#include <random>

/**
 * \class RandomGen
 * \brief Cette classe permet de générer un nombre aléatoire
 * */
class RandomGen
{
	public:
		/**
		 * @brief Permet de récupérer l'instance statique
		 * @return Instance statique
		 */
		static RandomGen& getInstance();

		/**
		 * @brief Renvoie un nombre aléatoire entre deux bornes
		 * @param a Première borne
		 * @param b Deuxième borne
		 * @return Un nombre flottant entre a et b
		 */
		double getDouble(const double &a, const double &b);

		/**
		 * @brief Renvoie un nombre aléatoire entre deux bornes
		 * @param a Première borne
		 * @param b Deuxième borne
		 * @return Un nombre entier entre a et b
		 */
		int    getInt(const int &a, const int &b);

	private:
		RandomGen();
		static std::mt19937_64 gen;
};

#endif // RANDOMLIB_HPP
