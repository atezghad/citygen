#define _USE_MATH_DEFINES
#include <sstream>
#include <fstream>
#include <cassert>
#include <cmath>
#include <algorithm>

#include <Utils/utils.hpp>
#include <Utils/Geometry/Vec2.hpp>
#include <Utils/Geometry/Vec3.hpp>
#include <Utils/Random.hpp>

#define PI_OVER_180 0.01745329251

std::vector<std::string> utils::split(const std::string &s, char delim)
{
	std::stringstream ss(s);
	std::string item;
	std::vector<std::string> tokens;
	while (std::getline(ss, item, delim))
	{
		tokens.push_back(item);
	}
	return tokens;
}

double utils::toRadians(const double &deg)
{
    return deg * PI_OVER_180;
}

double utils::toDegrees(const double &rad)
{
    return rad / PI_OVER_180;
}

std::string utils::noComment(std::ifstream &fichier)
{
	std::string ligne;
	do
	{
		std::getline(fichier, ligne);
	}
	while(!ligne.empty() && ligne.at(0) == '#');
	return ligne;
}

bool utils::form::compare_form(const char q, const char t)
{
	return (q == 'Q' && t == 'T');
}

bool utils::geom::validTriangle(const Point2 &a, const Point2 &b, const Point2 &c)
{
	Vec2 IN(a, b);
	Vec2 IP(a, c);
	return (IN.cross(IP).z() > 0);
}

bool utils::geom::intersectSegments2D(const Point2& a,const Point2 &b, const Point2 &x, const Point2 &y)
{
	if(a != x && a != y && b != x && b != y)
		return (validTriangle(a, x, y) != validTriangle(b, x, y) && validTriangle(a, b, x) != validTriangle(a, b, y));
	return false;
}

bool utils::geom::aligned(const Point2 &a, const Point2 &b, const Point2 &p)
{
	Vec2 ab(a, b);
	Vec2 ap(a, p);
	Vec3 abXap = ab.cross(ap);
	if(abXap.z() != .0)
		return false;

	double dot = ab.dot(ap);
	if(dot < .0)
		return false;
	double l = ab.norm2();

	if(dot > l)
		return false;
	return true;
}

Point2 utils::geom::center(const Point2 &a, const Point2 &b)
{
	return Point2(a + (Vec2(a, b))*.5);
}

Point3 utils::geom::center(const Point3 &a, const Point3 &b)
{
	return Point3(a + (Vec3(a, b))*.5);
}

bool utils::geom::onTriangle(const Point2 &a, const Point2 &b, const Point2 &c, const Point2 &p)
{
	return (aligned(a, b, p) || aligned(a, c, p) || aligned(b, c, p));
}

double utils::geom::triangleArea(const double &perimeter, const Point2 &p0, const Point2 &p1, const Point2 &p2)
{
	double s = perimeter/2.0;
	double a = p0.distance(p1);
	double b = p1.distance(p2);
	double c = p2.distance(p0);
	return std::sqrt(s*(s-a)*(s-b)*(s-c));
}

