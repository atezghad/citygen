#include <Utils/Random.hpp>
#include <chrono>

std::mt19937_64 RandomGen::gen;

RandomGen::RandomGen()
{
	unsigned int seed1 = std::chrono::system_clock::now().time_since_epoch().count();
	gen = std::mt19937_64(seed1);
}

RandomGen& RandomGen::getInstance()
{
	static RandomGen instance;
	return instance;
}

double RandomGen::getDouble(const double &a, const double &b)
{
	std::uniform_real_distribution<> dis(a, b);
	return dis(gen);
}

int RandomGen::getInt(const int &a, const int &b)
{
	std::uniform_int_distribution<> dis(a, b);
	return dis(gen);
}
