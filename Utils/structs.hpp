#ifndef STRUCTS_HPP
#define STRUCTS_HPP

/**
 * @brief Permet de définir dans quel ordre les Points d'une forme doivent être enregistrés dans un fichier .OBJ
 */
struct indexes
{
		unsigned int a;
		unsigned int b;
		unsigned int c;
};

/**
 * @brief permet de représenter simplement un volume 3D
 */
struct Volume
{
		double l;
		double L;
		double h;
};

/**
 * @brief Permet de contenir les dimensions principales d'un Element architectural
 */
struct Element
{
		double widthSpace;
		double lengthSpace;
		Volume volume;
		double thickness;
};

#endif // STRUCTS_HPP
