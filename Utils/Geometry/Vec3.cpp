#define _USE_MATH_DEFINES
#include <cmath>

#include <Utils/Geometry/Vec3.hpp>

Vec3 Vec3::X(1, 0, 0);
Vec3 Vec3::Y(0, 1, 0);
Vec3 Vec3::Z(0, 0, 1);

Vec3::Vec3() : Point3(0, 0, 0)
{}

Vec3::Vec3(const Point3 &a, const Point3 &b) : Point3(b.x()-a.x(),
															  b.y()-a.y(),
															  b.z()-a.z())
{}

Vec3::Vec3(const Point3 &a) : Point3(a)
{}

Vec3::Vec3(const double &x, const double &y, const double &z): Point3(x, y, z)
{}

bool Vec3::operator ==(const Vec3 &v) const
{
	return Point3::operator ==(v);
}

bool Vec3::operator !=(const Vec3 &v) const
{
	return Point3::operator !=(v);
}

double Vec3::dot(const Vec3 &v) const
{
	return  this->_x * v.x() +
			this->_y * v.y() +
			this->_z * v.z() ;
}

Vec3 Vec3::cross(const Vec3 &v) const
{
	double x = v.x();
	double y = v.y();
	double z = v.z();
	return Vec3(this->_y*z-this->_z*y,
				 this->_z*x-this->_x*z,
				 this->_x*y-this->_y*x
				 );

}

double Vec3::angleRad(const Vec3 &v) const
{
	double s = dot(v);
	double n = norm() * v.norm();
	if(n != 0)
		return std::acos(s/n);
	return 0;
}

double Vec3::angleDeg(const Vec3 &v) const
{
	return this->angleRad(v) * 180. / M_PI;
}

double Vec3::norm2() const
{
	return (this->_x*this->_x +
			this->_y*this->_y +
			this->_z*this->_z);
}

double Vec3::norm() const
{
	return std::sqrt(norm2());
}

Vec3 Vec3::normalize()
{
	Vec3 r = *this;
	if(norm() != 0.)
		r /= norm();
	return r;
}

axisAngle getAxisAngle(const Vec3 &a, const Vec3 &b)
{
	axisAngle aa;
	aa.axis = a.cross(b).normalize();
	aa.angle = a.angleRad(b);
	return aa;
}

std::ostream& operator<<(std::ostream& os, const Vec3& c)
{
	os << "(" << c._x << ", " << c._y << ", " << c._z  << ")";
	return os;
}
