/**
* \file Point3.hpp
* \author Anis Tezghadanti
* */
#ifndef COORDS_H
#define COORDS_H

#include <iostream>

/**
 * \class Point3
 * \brief Cette classe permet de représenter des coordonnées de manière géometrique en 3D
 * */
class Point3
{
	public:
		/**
		 * \brief Constructeur par défaut
		 * */
		Point3();

		/**
		 * \brief Constructeur
		 * \param[in] x Coordonnée en X
		 * \param[in] y Coordonnée en Y
		 * \param[in] z	Coordonnée en Z
		 * */
		Point3(const double &x, const double &y, const double &z);

		/**
		 * \brief Constructeur par copie
		 * \param[in] c Point3
		 * */
		Point3(const Point3 &c);

		/**
		 * \brief Constructeur par déplacement
		 * \param[in] c Point3
		 * */
		Point3(Point3 &&c);

		/**
		 * \brief Opérateur d'assignement
		 * \param[in] v Point3 à copier
		 * \return une référence de la copie de v
		 * */
		Point3& operator =(const Point3 &v);

		/**
		 * \brief Permet de comparer deux Point3
		 * \param[in] v Le Point3 à comparer
		 * \return true si v égal, false sinon
		 * */
		bool operator==(const Point3 &v) const ;

		/**
		 * \brief Permet de comparer deux Point3
		 * \param[in] v Le Point3 à comparer
		 * \return true si v différent, false sinon
		 * */
		bool operator!=(const Point3 &v) const;

		/**
		 * \brief Permet d'additionner deux Point3
		 * \param[in] a Point3
		 * \param[in] b Point3
		 * \return a+b
		 * */
		friend Point3 operator +(const Point3 &a, const Point3 &b);

		/**
		 * \brief Permet de soustraire deux Point3
		 * \param[in] a Point3
		 * \param[in] b Point3
		 * \return a-b
		 * */
		friend Point3 operator -(const Point3 &a, const Point3 &b);

		/**
		 * \brief Permet d'inverser un Point3
		 * \param[in] a Point3
		 * \return -a (-a.x, -a.y, -a.z)
		 * */
		friend Point3 operator -(const Point3 &a);

		/**
		 * \brief Permet de multiplier un Point3 par un scalaire
		 * \param[in] d scalaire
		 * \param[in] c Point3
		 * \return a*d (a.x*d, a.y*d, a.z*d)
		 * */
		friend Point3 operator *(const double &d, const Point3 &c);

		/**
		 * \brief Permet de diviser un Point3 par un scalaire
		 * \param[in] d scalaire
		 * \param[in] c Point3
		 * \return a/d (a.x/d, a.y/d, a.z/d)
		 * */
		friend Point3 operator /(const double &d, const Point3 &c);

		/**
		 * \brief Permet de multiplier un Point3 par un scalaire
		 * \param[in] d scalaire
		 * \param[in] c Point3
		 * \return a*d (a.x*d, a.y*d, a.z*d)
		 * */
		friend Point3 operator *(const Point3 &c, const double &d);

		/**
		 * \brief Permet de diviser un Point3 par un scalaire
		 * \param[in] d scalaire
		 * \param[in] c Point3
		 * \return a/d (a.x/d, a.y/d, a.z/d)
		 * */
		friend Point3 operator /(const Point3 &c, const double &d);

		/**
		 * \brief Permet d'additionner deux Point3
		 * \param[in] c Point3
		 * \return a = a+c
		 * */
		Point3& operator +=(const Point3 &c);

		/**
		 * \brief Permet de soustraire deux Point3
		 * \param[in] c Point3
		 * \return a = a-c
		 * */
		Point3& operator -=(const Point3 &c);

		/**
		 * \brief Permet de multiplier un Point3 par un scalaire
		 * \param[in] d scalaire
		 * \return a = a*d
		 * */
		Point3& operator *=(const double &d);

		/**
		 * \brief Permet de diviser un Point3 par un scalaire
		 * \param[in] d scalaire
		 * \return a = a/d
		 * */
		Point3 &operator /=(const double &d);

		/**
		 * \brief Opérateur de flux
		 * \param[in] c Point3
		 * \param[in][out] os
		 * \return Output stream : (x,y,z)
		 * */
		friend std::ostream& operator<<(std::ostream& os, const Point3& c);

		/**
		 * \brief Retorune la coordonnée en X
		 * \return la coordonnée en X
		 * */
		double x() const;

		/**
		 * \brief Retorune la coordonnée en Y
		 * \return la coordonnée en Y
		 * */
		double y() const;

		/**
		 * \brief Rotation autour d'un axe quelconque
		 * \param[in] axis L'axe de rotation
		 * \param[in] angle L'angle (en radians!)
		 * */
		void rotate(const Point3& axis, const double& angle);

		/**
		 * \brief Retorune la coordonnée en Z
		 * \return la coordonnée en Z
		 * */
		double z() const;

		/**
		 * \brief Modifie la coordonnée en X
		 * \param[in] x La nouvelle coordonnée en X
		 * */
		void setX(const double &x);

		/**
		 * \brief Modifie la coordonnée en Y
		 * \param[in] y La nouvelle coordonnée en Y
		 * */
		void setY(const double &y);

		/**
		 * \brief Modifie la coordonnée en Z
		 * \param[in] z La nouvelle coordonnée en Z
		 * */
		void setZ(const double &z);


		/**
		 * \brief Retourne la distance au carré entre 2 Coords2D
		 * \param[in] c Point3
		 * \return la distance au carré
		 * */
		double distanceSq(const Point3 &c) const;

		/**
		 * \brief Retourne la distance entre 2 Coords2D
		 * \param[in] c Point3
		 * \return la distance
		 * */
		double distance(const Point3 &c) const;


	protected:
		double _x;
		double _y;
		double _z;

};

#endif // COORDS_H
