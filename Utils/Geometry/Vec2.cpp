#define _USE_MATH_DEFINES
#include <cmath>

#include <Utils/Geometry/Vec2.hpp>


Vec2 Vec2::X(1,0);
Vec2 Vec2::Y(0,1);

Vec2::Vec2() : Point2(0, 0)
{}

Vec2::Vec2(const Point2 &a, const Point2 &b) : Point2(b.x()-a.x(),
															  b.y()-a.y())
{}

Vec2::Vec2(const Point2 &a) : Point2(a)
{}

Vec2::Vec2(const double &x, const double &y): Point2(x, y)
{}

bool Vec2::operator ==(const Vec2 &v) const
{
	return Point2::operator ==(v);
}

bool Vec2::operator !=(const Vec2 &v) const
{
	return Point2::operator !=(v);
}

double Vec2::dot(const Vec2 &v)
{
	return  this->_x * v.x() +
			this->_y * v.y() ;
}

Vec3 Vec2::cross(const Vec2 &v)
{
	double x = v.x();
	double y = v.y();
	return Vec3(.0, .0, this->_x*y-this->_y*x);

}

double Vec2::angleRad(const Vec2 &v)
{
	double s = dot(v);
	double n = norm() * v.norm();
	if(n != 0)
		return std::acos(s/n);
	return 0;
}

double Vec2::angleDeg(const Vec2 &v)
{
	return this->angleRad(v) * 180. / M_PI;
}

double Vec2::norm2() const
{
	return (this->_x*this->_x +
			this->_y*this->_y);
}

double Vec2::norm() const
{
	return std::sqrt(norm2());
}

Vec2 Vec2::normalize()
{
	Vec2 r = *this;
	if(norm() != 0.)
		r /= norm();
	return r;
}

std::ostream& operator<<(std::ostream& os, const Vec2& c)
{
	os << "(" << c._x << ", " << c._y << ")";
	return os;
}

Vec2 normal(const Point2 &a, const Point2 &b)
{
	Vec2 v(a, b);
	return Vec2(-v.y(), v.x()).normalize();
}
