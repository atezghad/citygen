/**
* \file Point2.hpp
* \author Anis Tezghadanti
* */
#ifndef POINT_H
#define POINT_H

#include <iostream>

/**
 * \class Point2
 * \brief Cette classe permet de représenter des coordonnées de manière géometrique en 2D
 * */
class Point2
{
	public:
		/**
		 * \brief Constructeur par défaut
		 * */
		Point2();

		/**
		 * \brief Constructeur
		 * \param[in] x Coordonnée en X
		 * \param[in] y Coordonnée en Y
		 * */
		Point2(const double &x, const double &y);

		/**
		 * \brief Constructeur par copie
		 * \param[in] c Point2
		 * */
		Point2(const Point2 &c);

		/**
		 * \brief Constructeur par déplacement
		 * \param[in] c Point2
		 * */
		Point2(Point2 &&c);

		/**
		 * \brief Opérateur d'assignement
		 * \param[in] v Point2 à copier
		 * \return une référence de la copie de v
		 * */
		Point2& operator =(const Point2 &v);

		/**
		 * \brief Permet de comparer deux Point2
		 * \param[in] v Le Point2 à comparer
		 * \return true si v égal, false sinon
		 * */
		bool operator==(const Point2 &v) const ;

		/**
		 * \brief Permet de comparer deux Point2
		 * \param[in] v Le Point2 à comparer
		 * \return true si v différent, false sinon
		 * */
		bool operator!=(const Point2 &v) const;

		/**
		 * \brief Permet d'additionner deux Point2
		 * \param[in] a Point2
		 * \param[in] b Point2
		 * \return a+b
		 * */
		friend Point2 operator +(const Point2 &a, const Point2 &b);

		/**
		 * \brief Permet de soustraire deux Point2
		 * \param[in] a Point2
		 * \param[in] b Point2
		 * \return a-b
		 * */
		friend Point2 operator -(const Point2 &a, const Point2 &b);

		/**
		 * \brief Permet d'inverser un Point2
		 * \param[in] a Point2
		 * \return -a (-a.x, -a.y)
		 * */
		friend Point2 operator -(const Point2 &a);

		/**
		 * \brief Permet de multiplier un Point2 par un scalaire
		 * \param[in] d scalaire
		 * \param[in] c Point2
		 * \return a*d (a.x*d, a.y*d)
		 * */
		friend Point2 operator *(const double &d, const Point2 &c);

		/**
		 * \brief Permet de diviser un Point2 par un scalaire
		 * \param[in] d scalaire
		 * \param[in] c Point2
		 * \return a/d (a.x/d, a.y/d)
		 * */
		friend Point2 operator /(const double &d, const Point2 &c);

		/**
		 * \brief Permet de multiplier un Point2 par un scalaire
		 * \param[in] d scalaire
		 * \param[in] c Point2
		 * \return a*d (a.x*d, a.y*d)
		 * */
		friend Point2 operator *(const Point2 &c, const double &d);

		/**
		 * \brief Permet de diviser un Point2 par un scalaire
		 * \param[in] d scalaire
		 * \param[in] c Point2
		 * \return a/d (a.x/d, a.y/d)
		 * */
		friend Point2 operator /(const Point2 &c, const double &d);

		/**
		 * \brief Permet d'additionner deux Point2
		 * \param[in] c Point2
		 * \return a = a+c
		 * */
		Point2& operator +=(const Point2 &c);

		/**
		 * \brief Permet de soustraire deux Point2
		 * \param[in] c Point2
		 * \return a = a-c
		 * */
		Point2& operator -=(const Point2 &c);

		/**
		 * \brief Permet de multiplier un Point2 par un scalaire
		 * \param[in] d scalaire
		 * \return a = a*d
		 * */
		Point2& operator *=(const double &d);

		/**
		 * \brief Permet de diviser un Point2 par un scalaire
		 * \param[in] d scalaire
		 * \return a = a/d
		 * */
		Point2 &operator /=(const double &d);

		/**
		 * \brief Opérateur de flux
		 * \param[in] c Point2
		 * \param[in][out] os
		 * \return Output stream : (x,y)
		 * */
		friend std::ostream& operator<<(std::ostream& os, const Point2& c);

		/**
		 * \brief Retourne la coordonnée en X
		 * \return la coordonnée en X
		 * */
		double x() const;

		/**
		 * \brief Retourne la coordonnée en Y
		 * \return la coordonnée en Y
		 * */
		double y() const;

		/**
		 * \brief Modifie la coordonnée en X
		 * \param[in] x La nouvelle coordonnée en X
		 * */
		void setX(const double &x);

		/**
		 * \brief Modifie la coordonnée en Y
		 * \param[in] y La nouvelle coordonnée en Y
		 * */
		void setY(const double &y);

		/**
		 * \brief Retourne la distance au carré entre 2 Point2
		 * \param[in] c Point2
		 * \return la distance au carré
		 * */
		double distanceSq(const Point2 &c) const;

		/**
		 * \brief Retourne la distance entre 2 Point2
		 * \param[in] c Point2
		 * \return la distance
		 * */
		double distance(const Point2 &c) const;


	protected:
		double _x;
		double _y;

};

/**
 * \brief Retourne un Point2 situé entre deux Point2
 * \param[in] a Point2
 * \param[in] b Point2
 * \return Point2 situé entre a et b
 * */
Point2 randomMid(const Point2 &a, const Point2 &b);

#endif // POINT_H
