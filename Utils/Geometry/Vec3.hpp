/**
* \file Vec3.hpp
* \author Anis Tezghadanti
* */
#ifndef Vec3_H
#define Vec3_H

#include <Utils/Geometry/Point3.hpp>

/**
 * \class Vec3
 * \brief Cette classe permet de représenter un vecteur en 3D
 * */
class Vec3 : public Point3
{
	public:
		/**
		 * \brief Constructeur par défaut
		 * */
		Vec3();

		/**
		 * \brief Constructeur
		 * \param[in] a Point3
		 * \param[in] b Point3
		 * */
		Vec3(const Point3 &a, const Point3 &b);

		/**
		 * \brief Constructeur
		 * \param[in] a Point3
		 * */
		Vec3(const Point3 &a);

		/**
		 * \brief Constructeur
		 * \param[in] x Coordonnée en X
		 * \param[in] y Coordonnée en Y
		 * \param[in] z	Coordonnée en Z
		 * */
		Vec3(const double &x, const double &y, const double &z);

		/**
		 * \brief Permet de comparer deux Vec3
		 * \param[in] v Le Vec3 à comparer
		 * \return true si v égal, false sinon
		 * */
		bool operator ==(const Vec3 &v) const;

		/**
		 * \brief Permet de comparer deux Vec3
		 * \param[in] v Le Vec3 à comparer
		 * \return true si v différent, false sinon
		 * */
		bool operator !=(const Vec3 &v) const;

		/**
		 * \brief Opérateur de flux
		 * \param[in] c Vec3
		 * \param[in][out] os
		 * \return Output stream : (x,y,z)
		 * */
		friend std::ostream& operator<<(std::ostream& os, const Vec3& c);

		/**
		 * \brief Retourne le produit scalaire Vec3.v
		 * \param[in] v Vec3
		 * \return Vec3.v
		 * */
		double dot(const Vec3 &v) const;

		/**
		 * \brief Retourne le produit vectoriel
		 * \param[in] v Vec3
		 * \return Vec3xv
		 * */
		Vec3 cross(const Vec3 &v) const;

		/**
		 * \brief Retourne l'angle en radians entre Vec3 et un autre vecteur
		 * \param[in] v Vec3
		 * \return L'angle en radians entre Vec3 et un autre vecteur
		 * */
		double angleRad(const Vec3 &v) const;

		/**
		 * \brief Retourne l'angle en degrés entre Vec3 et un autre vecteur
		 * \param[in] v Vec3
		 * \return L'angle en degrés entre Vec3 et un autre vecteur
		 * */
		double angleDeg(const Vec3 &v) const;

		/**
		 * \brief Retourne la norme du vecteur
		 * \return La norme du vecteur
		 * */
		double norm() const;

		/**
		 * \brief Retourne la norme au carré du vecteur
		 * \return La norme au carré du vecteur
		 * */
		double norm2() const;

		/**
		 * \brief Retourne le vecteur normalisé (norme = 1)
		 * \return Le vecteur normalisé
		 * */
		Vec3 normalize();

		/**
		 * \brief X = (1,0,0)
		 * */
		static Vec3 X;
		/**
		 * \brief Y = (0,1,0)
		 * */
		static Vec3 Y;
		/**
		 * \brief Z = (0,0,1)
		 * */
		static Vec3 Z;

};
/**
 * @brief axisAngle struct
 *
 */
struct axisAngle
{
		Vec3 axis;
		double angle;
};

/**
 * \brief Permet de récupérer l'angle et l'axe de rotation entre deux Vec
 * \param[in] a Vec3
 * \param[in] b Vec3
 * \return L'angle et l'axe de rotation entre a et b
 * */
axisAngle getAxisAngle(const Vec3 &a, const Vec3 &b);

#endif // Vec3_H
