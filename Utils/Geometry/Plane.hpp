/**
* \file Plane.hpp
* \author Anis Tezghadanti
* */
#ifndef PLANE_H
#define PLANE_H

#include <Utils/Geometry/Vec3.hpp>

/**
 * @brief The Plane struct \n Permet de réprésenter un Plan
 */
struct Plane
{
		Point3 center;
		Vec3 x;
		Vec3 y;
		Vec3 n;
		bool normalInside;
};

/**
 * @brief Permet de construire un Plan
 * @param a Origine du plan (AB, AC)
 * @param b Un point du plan
 * @param c Un point du plan
 * @return Un plan avec vecteurs directeurs normalisés plus un vecteur normal
 */
Plane makePlane(const Point3 &a, const Point3 &b, const Point3 &c);
/**
 * @brief makePlane
 * @param c Le centre du plan
 * @param x Vecteur directeur
 * @param y Vecteur directeur
 * @param n Vecteur normal
 * @return Un plan
 */
Plane makePlane(const Point3 &c, const Vec3 &x, const Vec3 &y, const Vec3 &n);
#endif // PLANE_H
