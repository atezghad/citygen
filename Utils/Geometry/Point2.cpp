#include <cmath>
#include <Utils/Geometry/Point2.hpp>
#include <Utils/Random.hpp>

Point2::Point2() : _x(0), _y(0)
{}

Point2::Point2(const double &x, const double &y) : _x(x), _y(y)
{}

Point2::Point2(const Point2 &c) : _x(c._x), _y(c._y)
{}

Point2::Point2(Point2 &&c) : _x(c._x), _y(c._y)
{
	c._x = 0;
	c._y = 0;
}

double Point2::x() const
{
	return _x;
}

double Point2::y() const
{
	return _y;
}

void Point2::setX(const double &x)
{
	_x = x;
}

void Point2::setY(const double &y)
{
	_y = y;
}

double Point2::distanceSq(const Point2 &c) const
{
	double mag_x = std::abs(_x - c._x);
	mag_x*=mag_x;
	double mag_y = std::abs(_y - c._y);
	mag_y*=mag_y;

	return mag_x+mag_y;
}

double Point2::distance(const Point2 &c) const
{
	return std::sqrt(distanceSq(c));
}

Point2& Point2::operator =(const Point2 &v)
{
	_x = v._x;
	_y = v._y;
	return *this;
}

bool Point2::operator==(const Point2 &v) const
{
	return (_x == v._x && _y == v._y);
}

bool Point2::operator!=(const Point2 &v) const
{
	return (!(*this == v));
}

Point2 operator +(const Point2 &a, const Point2 &b)
{
	return Point2(a.x() + b.x(), a.y() + b.y());
}

Point2 operator -(const Point2 &a, const Point2 &b)
{
	return Point2(a.x() - b.x(), a.y() - b.y());
}

Point2 operator -(const Point2 &a)
{
	return Point2(-a.x(), -a.y());
}

Point2 operator *(const double &d, const Point2 &c)
{
	return Point2(c.x()*d, c.y()*d);
}

Point2 operator /(const double &d, const Point2 &c)
{
	return (1./d)*c;
}

Point2 operator *(const Point2 &c, const double &d)
{
	return Point2(c.x()*d, c.y()*d);
}

Point2 operator /(const Point2 &c, const double &d)
{
	return (1./d)*c;
}

Point2& Point2::operator +=(const Point2 &c)
{
	*this = *this + c;
	return *this;
}

Point2& Point2::operator -=(const Point2 &c)
{
	*this = *this - c;
	return *this;
}

Point2& Point2::operator *=(const double &d)
{
	*this = *this * d;
	return *this;
}

Point2& Point2::operator /=(const double &d)
{
	*this = *this / d;
	return *this;
}

std::ostream& operator<<(std::ostream& os, const Point2& c)
{
	os << "(" << c._x << ", " << c._y << ")";
	return os;
}

Point2 randomMid(const Point2 &a, const Point2 &b)
{
    double t = RandomGen::getInstance().getInt(45, 55) / 100.0;
	Point2 vec = t *(b - a);
	return Point2(a + vec);
}
