#include <cmath>

#include <Utils/Geometry/Point3.hpp>
#include <Utils/Geometry/Vec3.hpp>

Point3::Point3() : _x(0), _y(0), _z(0)
{}

Point3::Point3(const double &x, const double &y, const double &z) : _x(x), _y(y), _z(z)
{}

Point3::Point3(const Point3 &c) : _x(c._x), _y(c._y), _z(c._z)
{}

Point3::Point3(Point3 &&c) : _x(c._x), _y(c._y), _z(c._z)
{
	c._x = 0;
	c._y = 0;
	c._z = 0;
}

double Point3::x() const
{
	return this->_x;
}

double Point3::y() const
{
	return this->_y;
}

double Point3::z() const
{
	return this->_z;
}

void Point3::setX(const double &x)
{
	_x = x;
}

void Point3::setY(const double &y)
{
	_y = y;
}

void Point3::setZ(const double &z)
{
	_z = z;
}

Point3& Point3::operator =(const Point3 &v)
{
	this->_x = v._x;
	this->_y = v._y;
	this->_z = v._z;
	return *this;
}

bool Point3::operator==(const Point3 &v) const
{
	return (this->_x == v._x && this->_y == v._y && this->_z == v._z);
}

bool Point3::operator!=(const Point3 &v) const
{
	return (!(*this == v));
}

Point3 operator +(const Point3 &a, const Point3 &b)
{
	return Point3(a.x() + b.x(), a.y() + b.y(), a.z() + b.z());
}

Point3 operator -(const Point3 &a, const Point3 &b)
{
	return Point3(a.x() - b.x(), a.y() - b.y(), a.z() - b.z());
}

Point3 operator -(const Point3 &a)
{
	return Point3(-a.x(), -a.y(), -a.z());
}

Point3 operator *(const double &d, const Point3 &c)
{
	return Point3(c.x()*d, c.y()*d, c.z()*d);
}

Point3 operator /(const double &d, const Point3 &c)
{
	return (1./d)*c;
}

Point3 operator *(const Point3 &c, const double &d)
{
	return Point3(c.x()*d, c.y()*d, c.z()*d);
}

Point3 operator /(const Point3 &c, const double &d)
{
	return (1./d)*c;
}

Point3& Point3::operator +=(const Point3 &c)
{
	*this = *this + c;
	return *this;
}

Point3& Point3::operator -=(const Point3 &c)
{
	*this = *this - c;
	return *this;
}

Point3& Point3::operator *=(const double &d)
{
	*this = *this * d;
	return *this;
}

Point3& Point3::operator /=(const double &d)
{
	*this = *this / d;
	return *this;
}

double Point3::distanceSq(const Point3 &c) const
{
	double mag_x = std::abs(_x - c._x);
	mag_x*=mag_x;
	double mag_y = std::abs(_y - c._y);
	mag_y*=mag_y;
	double mag_z = std::abs(_z - c._z);
	mag_z*=mag_z;
	return mag_x+mag_y+mag_z;
}

double Point3::distance(const Point3 &c) const
{
	return std::sqrt(distanceSq(c));
}

void Point3::rotate(const Point3& axis, const double& angle)
{
	double cos = std::cos(angle);
	if(std::abs(cos) < 0.000001)
		cos = 0;
	double un_cos = 1.0 - cos;
	double sin = std::sin(angle);
	if(std::abs(sin) < 0.000001)
		sin = 0;

	double zz = cos + axis.x()*axis.x()*un_cos;
	double zu = axis.x()*axis.y()*un_cos - axis.z()*sin;
	double zd = axis.x()*axis.z()*un_cos + axis.y()*sin;

	double uz = axis.x()*axis.y()*un_cos + axis.z()*sin;
	double uu = cos + axis.y()*axis.y()*un_cos ;
	double ud = axis.y()*axis.z()*un_cos - axis.x()*sin;

	double dz = axis.x()*axis.z()*un_cos - axis.y()*sin;
	double du = axis.y()*axis.z()*un_cos + axis.x()*sin;
	double dd = cos + axis.z()*axis.z()*un_cos ;


	double x = zz*_x + zu*_y + zd*_z;
	double y = uz*_x + uu*_y + ud*_z;
	double z = dz*_x + du*_y + dd*_z;

	if(std::abs(x) < 0.000001)
		x = 0;
	if(std::abs(y) < 0.000001)
		y = 0;
	if(std::abs(z) < 0.000001)
		z = 0;

	_x = x;
	_y = y;
	_z = z;
}

std::ostream& operator<<(std::ostream& os, const Point3& c)
{
	os << "(" << c._x << ", " << c._y << ", " << c._z  << ")";
	return os;
}
