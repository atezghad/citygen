#include <Utils/Geometry/Plane.hpp>
#include <Utils/utils.hpp>


Plane makePlane(const Point3 &a, const Point3 &b, const Point3 &c)
{
	Plane p;
	p.normalInside = a.x() <= b.x() && a.z() <= b.z();
	p.center = utils::geom::center(b, c);
	p.x = (p.normalInside)? Vec3(a, b).normalize() : Vec3(b, a).normalize();
	p.y = Vec3(a, c).normalize() ;
	p.n = p.x.cross(p.y).normalize();
	return p;
}

Plane makePlane(const Point3 &c, const Vec3 &x, const Vec3 &y, const Vec3 &n)
{
	Plane p;
	p.center = c;
	p.x = x;
	p.y =y ;
	p.n = n;
	p.normalInside = true;
	return p;
}

