/**
* \file Vec2.hpp
* \author Anis Tezghadanti
* */
#ifndef Vec2_H
#define Vec2_H

#include <Utils/Geometry/Point2.hpp>
#include <Utils/Geometry/Vec3.hpp>

/**
 * \class Vec2
 * \brief Cette classe permet de représenter un vecteur en 2D
 * */
class Vec2 : public Point2
{
	public:
		/**
		 * \brief Constructeur par défaut
		 * */
		Vec2();

		/**
		 * \brief Constructeur
		 * \param[in] a Point2
		 * \param[in] b Point2
		 * */
		Vec2(const Point2 &a, const Point2 &b);

		/**
		 * \brief Constructeur
		 * \param[in] a Point2
		 * */
		Vec2(const Point2 &a);

		/**
		 * \brief Constructeur
		 * \param[in] x Coordonnée en X
		 * \param[in] y Coordonnée en Y
		 * */
		Vec2(const double &x, const double &y);

		/**
		 * \brief Permet de comparer deux Vec2
		 * \param[in] v Le Vec2 à comparer
		 * \return true si v égal, false sinon
		 * */
		bool operator ==(const Vec2 &v) const;

		/**
		 * \brief Permet de comparer deux Vec2
		 * \param[in] v Le Vec2 à comparer
		 * \return true si v différent, false sinon
		 * */
		bool operator !=(const Vec2 &v) const;

		/**
		 * \brief Opérateur de flux
		 * \param[in] c Vec2
		 * \param[in][out] os
		 * \return Output stream : (x,y)
		 * */
		friend std::ostream& operator<<(std::ostream& os, const Vec2& c);

		/**
		 * \brief Retourne le produit scalaire Vec2.v
		 * \param[in] v Vec2
		 * \return Vec2.v
		 * */
		double dot(const Vec2 &v);

		/**
		 * \brief Retourne le produit vectoriel
		 * \param[in] v Vec2
		 * \return Vec2xv
		 * */
		Vec3 cross(const Vec2 &v);

		/**
		 * \brief Retourne l'angle en radians entre Vec2 et un autre vecteur
		 * \param[in] v Vec2
		 * \return L'angle en radians entre Vec2 et un autre vecteur
		 * */
		double angleRad(const Vec2 &v);

		/**
		 * \brief Retourne l'angle en degrés entre Vec2 et un autre vecteur
		 * \param[in] v Vec2
		 * \return L'angle en degrés entre Vec2 et un autre vecteur
		 * */
		double angleDeg(const Vec2 &v);

		/**
		 * \brief Retourne la norme du vecteur
		 * \return La norme du vecteur
		 * */
		double norm() const;

		/**
		 * \brief Retourne la norme au carré du vecteur
		 * \return La norme au carré du vecteur
		 * */
		double norm2() const;

		/**
		 * \brief Retourne le vecteur normalisé (norme = 1)
		 * \return Le vecteur normalisé
		 * */
		Vec2 normalize();

		static Vec2 X;
		static Vec2 Y;
};

Vec2 normal(const Point2 &a, const Point2 &b);
#endif // Vec2_H
